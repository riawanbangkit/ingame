-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2018 at 04:32 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ingame`
--

-- --------------------------------------------------------

--
-- Table structure for table `beli_material`
--

CREATE TABLE `beli_material` (
  `id_bMtrl` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `id_pkt` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total_cost` int(20) NOT NULL,
  `id_trm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beli_material`
--

INSERT INTO `beli_material` (`id_bMtrl`, `id_team`, `id_pkt`, `jumlah`, `total_cost`, `id_trm`) VALUES
(1, 8, 2, 2, 4350000, 1),
(2, 5, 1, 1, 1450000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id_cty` int(11) NOT NULL,
  `city_name` varchar(250) NOT NULL,
  `type` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id_cty`, `city_name`, `type`) VALUES
(1, 'Pekalongan', 1),
(2, 'Jakarta', 2),
(3, 'Bandung', 2),
(4, 'Cirebon', 1),
(5, 'Tangerang', 2);

-- --------------------------------------------------------

--
-- Table structure for table `demand`
--

CREATE TABLE `demand` (
  `id_dmd` int(11) NOT NULL,
  `id_trm` int(11) NOT NULL,
  `id_prd` int(11) NOT NULL,
  `id_cty` int(11) NOT NULL,
  `demand` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demand`
--

INSERT INTO `demand` (`id_dmd`, `id_trm`, `id_prd`, `id_cty`, `demand`) VALUES
(1, 1, 1, 2, 15),
(2, 1, 2, 2, 15),
(3, 1, 1, 3, 35),
(4, 2, 1, 2, 50),
(5, 2, 2, 2, 30),
(6, 1, 2, 3, 25);

-- --------------------------------------------------------

--
-- Table structure for table `denda`
--

CREATE TABLE `denda` (
  `id_denda` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `denda` decimal(20,2) NOT NULL,
  `id_trm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `denda`
--

INSERT INTO `denda` (`id_denda`, `id_team`, `denda`, `id_trm`) VALUES
(1, 8, '30000.45', 1),
(2, 5, '200000.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `detail_jmtrl`
--

CREATE TABLE `detail_jmtrl` (
  `id_dtl` int(11) NOT NULL,
  `id_jMtrl` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `id_raw` int(11) NOT NULL,
  `raw` int(11) NOT NULL,
  `jPrice` int(20) NOT NULL,
  `id_trm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_jmtrl`
--

INSERT INTO `detail_jmtrl` (`id_dtl`, `id_jMtrl`, `id_team`, `id_raw`, `raw`, `jPrice`, `id_trm`) VALUES
(106, 5, 5, 2, 2, 500000, 1),
(107, 5, 5, 3, 2, 300000, 1),
(108, 5, 5, 4, 2, 20000, 1),
(109, 5, 5, 5, 2, 20000, 1),
(110, 5, 5, 6, 2, 15000, 1),
(111, 5, 5, 7, 2, 2000, 1),
(112, 5, 5, 8, 3, 1500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `iklan`
--

CREATE TABLE `iklan` (
  `id_pIklan` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `id_iklan` int(11) NOT NULL,
  `durasi` int(11) NOT NULL,
  `total_cost` int(20) NOT NULL,
  `id_trm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklan`
--

INSERT INTO `iklan` (`id_pIklan`, `id_team`, `id_iklan`, `durasi`, `total_cost`, `id_trm`) VALUES
(3, 6, 1, 5, 1000000, 1),
(4, 7, 2, 2, 1000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `iklan_type`
--

CREATE TABLE `iklan_type` (
  `id_iklan` int(11) NOT NULL,
  `iklan_type` varchar(250) NOT NULL,
  `iklan_price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklan_type`
--

INSERT INTO `iklan_type` (`id_iklan`, `iklan_type`, `iklan_price`) VALUES
(1, 'Iklan Broadcast Monitor', 200000),
(2, 'Iklan Putaran Jalan', 500000);

-- --------------------------------------------------------

--
-- Table structure for table `jual_material`
--

CREATE TABLE `jual_material` (
  `id_jMtrl` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `total_cost` int(20) DEFAULT NULL,
  `id_trm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jual_material`
--

INSERT INTO `jual_material` (`id_jMtrl`, `id_team`, `total_cost`, `id_trm`) VALUES
(5, 5, 858500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `koran`
--

CREATE TABLE `koran` (
  `id_koran` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `qty_koran` int(11) NOT NULL,
  `total_cost` int(20) NOT NULL,
  `id_trm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `id_pkt` int(11) NOT NULL,
  `paket_name` varchar(250) NOT NULL,
  `paket_price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id_pkt`, `paket_name`, `paket_price`) VALUES
(1, 'Paket A', 1450000),
(2, 'Paket B', 2175000);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id_pjl` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `qty_reg` int(11) NOT NULL,
  `qty_prm` int(11) NOT NULL,
  `cty_to` int(11) NOT NULL,
  `id_tc` int(11) NOT NULL,
  `id_trm` int(11) NOT NULL,
  `reg_total` int(20) NOT NULL,
  `prm_total` int(20) NOT NULL,
  `tc_total` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id_pjl`, `id_team`, `qty_reg`, `qty_prm`, `cty_to`, `id_tc`, `id_trm`, `reg_total`, `prm_total`, `tc_total`) VALUES
(3, 5, 10, 10, 2, 2, 1, 3000000, 5000000, 2000),
(4, 8, 10, 10, 2, 1, 1, 3000000, 5000000, 1600),
(5, 8, 10, 0, 3, 4, 1, 3100000, 0, 2200),
(6, 8, 10, 0, 2, 2, 1, 3000000, 0, 2000),
(7, 5, 10, 5, 2, 2, 1, 3000000, 2500000, 2000),
(8, 8, 20, 0, 3, 3, 1, 6200000, 0, 1500),
(9, 8, 20, 5, 2, 2, 1, 6000000, 2500000, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `pinjaman`
--

CREATE TABLE `pinjaman` (
  `id_pjm` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `pinjaman` int(20) NOT NULL,
  `id_trm` int(11) NOT NULL,
  `status_pjm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pinjaman`
--

INSERT INTO `pinjaman` (`id_pjm`, `id_team`, `pinjaman`, `id_trm`, `status_pjm`) VALUES
(1, 6, 5000000, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE `price` (
  `id_price` int(11) NOT NULL,
  `id_prd` int(11) NOT NULL,
  `id_trm` int(11) NOT NULL,
  `id_cty` int(11) NOT NULL,
  `price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price`
--

INSERT INTO `price` (`id_price`, `id_prd`, `id_trm`, `id_cty`, `price`) VALUES
(1, 1, 1, 2, 300000),
(2, 2, 1, 2, 500000),
(3, 1, 1, 3, 310000),
(4, 2, 1, 3, 550000),
(5, 1, 2, 2, 300000),
(6, 2, 2, 2, 500000);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id_prd` int(11) NOT NULL,
  `product_name` varchar(150) NOT NULL,
  `product_desc` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_prd`, `product_name`, `product_desc`) VALUES
(1, 'Regular', 'Produk Reguler'),
(2, 'Premium', 'Produk Premium');

-- --------------------------------------------------------

--
-- Table structure for table `rawmaterial`
--

CREATE TABLE `rawmaterial` (
  `id_raw` int(11) NOT NULL,
  `raw_name` varchar(250) NOT NULL,
  `raw_price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rawmaterial`
--

INSERT INTO `rawmaterial` (`id_raw`, `raw_name`, `raw_price`) VALUES
(2, 'Duplex Besar (8x8)', 250000),
(3, 'Duplex Kecil (6x6)', 150000),
(4, 'Mur Flens', 10000),
(5, 'Hex Bolt 7cm', 10000),
(6, 'Hex Bolt 4cm', 7500),
(7, 'Mur Biasa', 1000),
(8, 'Ring', 500);

-- --------------------------------------------------------

--
-- Table structure for table `saldo_akhir`
--

CREATE TABLE `saldo_akhir` (
  `id_sa` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `hold` decimal(20,2) NOT NULL,
  `simpanan` decimal(20,2) NOT NULL,
  `pinjaman` decimal(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saldo_akhir`
--

INSERT INTO `saldo_akhir` (`id_sa`, `id_team`, `hold`, `simpanan`, `pinjaman`) VALUES
(1, 5, '16754500.00', '0.00', '0.00'),
(2, 6, '3575000.00', '0.00', '0.00'),
(3, 7, '4000000.00', '0.00', '0.00'),
(4, 8, '756699.55', '30000000.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `sewa`
--

CREATE TABLE `sewa` (
  `id_sewa` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `provider` int(11) NOT NULL,
  `cost` int(20) NOT NULL,
  `id_trm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sewa`
--

INSERT INTO `sewa` (`id_sewa`, `customer`, `provider`, `cost`, `id_trm`) VALUES
(1, 5, 6, 200000, 1),
(2, 8, 7, 150000, 1),
(3, 5, 7, 250000, 1),
(4, 5, 6, 500000, 1),
(5, 8, 7, 1000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_menu`
--

CREATE TABLE `system_menu` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `parent_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `id_pm` int(11) NOT NULL,
  `menu_name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `icon` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `menu_order` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `system_menu`
--

INSERT INTO `system_menu` (`id`, `parent_id`, `id_pm`, `menu_name`, `url`, `icon`, `menu_order`) VALUES
(4, 0, 2, 'Cpanel', '', 'fa fa-bars', 4),
(6, 4, 2, 'Account', 'cpanel/account', '', 3),
(11, 4, 2, 'Access Rights', 'cpanel/accessRights', '', 0),
(8, 4, 2, 'Menu', 'cpanel/menu', '', 4),
(22, 21, 1, 'Penjualan', 'transaction/penjualan', '', 1),
(20, 0, 3, 'Setting', '', 'fa fa-gears', 3),
(21, 0, 1, 'Transaction', '', 'fa fa-money', 2),
(25, 20, 3, 'Termin', 'settings/termin', '', 1),
(29, 0, 4, 'Dashboard', 'main/dashboard', 'fa fa-desktop', 0),
(30, 21, 1, 'Koran', 'transaction/koran', '', 2),
(33, 21, 1, 'Beli Material', 'transaction/beliMtrl', '', 5),
(34, 21, 1, 'Jual Material', 'transaction/jualMtrl', '', 6),
(35, 21, 1, 'Pinjam Bank', 'transaction/pinjaman', '', 8),
(36, 21, 1, 'Tabungan Bank', 'transaction/tabungan', '', 7),
(37, 21, 1, 'Iklan', 'transaction/iklan', '', 3),
(38, 21, 1, 'Denda / Tax', 'transaction/denda', '', 4),
(39, 20, 3, 'City', 'settings/cities', '', 2),
(40, 20, 3, 'Product', 'settings/product', '', 3),
(41, 20, 3, 'Raw Material', 'settings/rawmaterial', '', 4),
(42, 20, 3, 'Demand', 'settings/demand', '', 6),
(43, 20, 3, 'Price', 'settings/price', '', 7),
(44, 20, 3, 'Transportation Cost', 'settings/transCost', '', 8),
(45, 20, 3, 'Team', 'settings/team', '', 9),
(46, 21, 1, 'Sewa Outsource', 'transaction/sewaOutsource', '', 11),
(47, 21, 1, 'Sewa Logistik', 'transaction/sewaLogistik', '', 12),
(48, 20, 3, 'Paket Raw Material', 'settings/paket', '', 5),
(49, 20, 3, 'Iklan', 'settings/iklanType', '', 10),
(50, 21, 1, 'Pembayaran Pinjaman', 'transaction/bayarPinjaman', '', 10),
(51, 21, 1, 'Penarikan Tabungan', 'transaction/tarik', '', 9),
(52, 0, 5, 'Display', '', 'fa fa-laptop', 1),
(53, 52, 5, 'Display Demand', 'display/demand', '', 1),
(54, 52, 5, 'Team Rank', 'display/rank', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `system_module`
--

CREATE TABLE `system_module` (
  `id_pm` int(11) NOT NULL,
  `module_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_module`
--

INSERT INTO `system_module` (`id_pm`, `module_name`) VALUES
(1, 'Transaction'),
(2, 'Cpanel'),
(3, 'Registrasi'),
(4, 'Dashboard'),
(5, 'Display');

-- --------------------------------------------------------

--
-- Table structure for table `system_user`
--

CREATE TABLE `system_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `display_path` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `level` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_user`
--

INSERT INTO `system_user` (`id_user`, `username`, `password`, `display_name`, `display_path`, `email`, `level`) VALUES
(1, 'entry', '1043bfc77febe75fafec0c4309faccf1', 'Data Entry', '', '', 2),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_user_level`
--

CREATE TABLE `system_user_level` (
  `id_level` int(11) NOT NULL,
  `level_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_user_level`
--

INSERT INTO `system_user_level` (`id_level`, `level_name`) VALUES
(1, 'Admin IT'),
(2, 'Data Entry');

-- --------------------------------------------------------

--
-- Table structure for table `system_user_rights`
--

CREATE TABLE `system_user_rights` (
  `id_up` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_pm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_user_rights`
--

INSERT INTO `system_user_rights` (`id_up`, `id_level`, `id_pm`) VALUES
(1, 1, 1),
(2, 1, 2),
(11, 1, 3),
(12, 1, 4),
(13, 1, 5),
(17, 4, 4),
(18, 2, 1),
(19, 2, 4),
(20, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `system_user_role`
--

CREATE TABLE `system_user_role` (
  `id_ur` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_user_role`
--

INSERT INTO `system_user_role` (`id_ur`, `id_user`, `id_level`) VALUES
(1, 1, 1),
(2, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tabungan`
--

CREATE TABLE `tabungan` (
  `id_tab` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `tab_type` int(11) NOT NULL,
  `tabungan` int(20) NOT NULL,
  `id_trm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabungan`
--

INSERT INTO `tabungan` (`id_tab`, `id_team`, `tab_type`, `tabungan`, `id_trm`) VALUES
(2, 6, 1, 1000000, 1),
(3, 6, 2, 500000, 1),
(4, 6, 2, 500000, 1),
(5, 8, 1, 30000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id_team` int(11) NOT NULL,
  `team_name` varchar(250) NOT NULL,
  `id_tt` int(11) NOT NULL,
  `team_capital` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id_team`, `team_name`, `id_tt`, `team_capital`) VALUES
(5, 'MajuJaya', 1, 5000000),
(6, 'Bundo Mananti', 2, 4000000),
(7, 'Translog', 3, 3600000),
(8, 'MaveTech', 1, 7500000);

-- --------------------------------------------------------

--
-- Table structure for table `team_type`
--

CREATE TABLE `team_type` (
  `id_tt` int(11) NOT NULL,
  `team_type` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_type`
--

INSERT INTO `team_type` (`id_tt`, `team_type`) VALUES
(1, 'Manufacture'),
(2, 'Outsource'),
(3, 'Logistics');

-- --------------------------------------------------------

--
-- Table structure for table `termin`
--

CREATE TABLE `termin` (
  `id_trm` int(11) NOT NULL,
  `termin` int(11) NOT NULL,
  `status_trm` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `termin`
--

INSERT INTO `termin` (`id_trm`, `termin`, `status_trm`) VALUES
(1, 1, 1),
(2, 2, 0),
(3, 3, 0),
(4, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transportation_cost`
--

CREATE TABLE `transportation_cost` (
  `id_tc` int(11) NOT NULL,
  `fromcty` int(11) NOT NULL,
  `tocty` int(11) NOT NULL,
  `id_trm` int(11) NOT NULL,
  `cost` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transportation_cost`
--

INSERT INTO `transportation_cost` (`id_tc`, `fromcty`, `tocty`, `id_trm`, `cost`) VALUES
(1, 4, 2, 1, 1600),
(2, 1, 2, 1, 2000),
(3, 4, 3, 1, 1500),
(4, 1, 3, 1, 2200),
(5, 1, 2, 2, 2000),
(6, 4, 2, 2, 1700),
(7, 4, 5, 1, 2000),
(8, 4, 3, 2, 1600),
(9, 4, 5, 2, 2000),
(10, 1, 5, 1, 2500);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_system_menu`
-- (See below for the actual view)
--
CREATE TABLE `view_system_menu` (
`id` tinyint(3) unsigned
,`parent_id` tinyint(3) unsigned
,`id_pm` int(11)
,`menu_name` varchar(100)
,`url` varchar(100)
,`icon` varchar(100)
,`menu_order` tinyint(3) unsigned
,`parent_name` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_system_user`
-- (See below for the actual view)
--
CREATE TABLE `view_system_user` (
`id_user` int(11)
,`level` int(2)
,`username` varchar(255)
,`password` varchar(255)
,`display_name` varchar(255)
,`display_path` varchar(255)
,`level_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_system_user_rights`
-- (See below for the actual view)
--
CREATE TABLE `view_system_user_rights` (
`id_up` int(11)
,`id_level` int(11)
,`id_pm` int(11)
,`id` tinyint(3) unsigned
,`parent_id` tinyint(3) unsigned
,`menu_name` varchar(100)
,`url` varchar(100)
,`icon` varchar(100)
,`menu_order` tinyint(3) unsigned
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_system_user_right_module`
-- (See below for the actual view)
--
CREATE TABLE `view_system_user_right_module` (
`id_up` int(11)
,`id_level` int(11)
,`id_pm` int(11)
,`module_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_system_user_role`
-- (See below for the actual view)
--
CREATE TABLE `view_system_user_role` (
`id_ur` int(11)
,`id_user` int(11)
,`id_level` int(11)
,`level_name` varchar(255)
,`username` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `view_system_menu`
--
DROP TABLE IF EXISTS `view_system_menu`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_system_menu`  AS  select `a`.`id` AS `id`,`a`.`parent_id` AS `parent_id`,`a`.`id_pm` AS `id_pm`,`a`.`menu_name` AS `menu_name`,`a`.`url` AS `url`,`a`.`icon` AS `icon`,`a`.`menu_order` AS `menu_order`,`b`.`menu_name` AS `parent_name` from (`system_menu` `a` left join `system_menu` `b` on((`a`.`parent_id` = `b`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_system_user`
--
DROP TABLE IF EXISTS `view_system_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_system_user`  AS  select `a`.`id_user` AS `id_user`,`a`.`level` AS `level`,`a`.`username` AS `username`,`a`.`password` AS `password`,`a`.`display_name` AS `display_name`,`a`.`display_path` AS `display_path`,`b`.`level_name` AS `level_name` from (`system_user` `a` join `system_user_level` `b` on((`a`.`level` = `b`.`id_level`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_system_user_rights`
--
DROP TABLE IF EXISTS `view_system_user_rights`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_system_user_rights`  AS  select `a`.`id_up` AS `id_up`,`a`.`id_level` AS `id_level`,`a`.`id_pm` AS `id_pm`,`b`.`id` AS `id`,`b`.`parent_id` AS `parent_id`,`b`.`menu_name` AS `menu_name`,`b`.`url` AS `url`,`b`.`icon` AS `icon`,`b`.`menu_order` AS `menu_order` from (`system_user_rights` `a` join `system_menu` `b` on((`a`.`id_pm` = `b`.`id_pm`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_system_user_right_module`
--
DROP TABLE IF EXISTS `view_system_user_right_module`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_system_user_right_module`  AS  select `a`.`id_up` AS `id_up`,`a`.`id_level` AS `id_level`,`a`.`id_pm` AS `id_pm`,`b`.`module_name` AS `module_name` from (`system_user_rights` `a` join `system_module` `b` on((`a`.`id_pm` = `b`.`id_pm`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_system_user_role`
--
DROP TABLE IF EXISTS `view_system_user_role`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_system_user_role`  AS  select `a`.`id_ur` AS `id_ur`,`a`.`id_user` AS `id_user`,`a`.`id_level` AS `id_level`,`b`.`level_name` AS `level_name`,`c`.`username` AS `username` from ((`system_user_role` `a` join `system_user_level` `b` on((`a`.`id_level` = `b`.`id_level`))) join `system_user` `c` on((`a`.`id_user` = `c`.`id_user`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beli_material`
--
ALTER TABLE `beli_material`
  ADD PRIMARY KEY (`id_bMtrl`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id_cty`);

--
-- Indexes for table `demand`
--
ALTER TABLE `demand`
  ADD PRIMARY KEY (`id_dmd`);

--
-- Indexes for table `denda`
--
ALTER TABLE `denda`
  ADD PRIMARY KEY (`id_denda`);

--
-- Indexes for table `detail_jmtrl`
--
ALTER TABLE `detail_jmtrl`
  ADD PRIMARY KEY (`id_dtl`);

--
-- Indexes for table `iklan`
--
ALTER TABLE `iklan`
  ADD PRIMARY KEY (`id_pIklan`);

--
-- Indexes for table `iklan_type`
--
ALTER TABLE `iklan_type`
  ADD PRIMARY KEY (`id_iklan`);

--
-- Indexes for table `jual_material`
--
ALTER TABLE `jual_material`
  ADD PRIMARY KEY (`id_jMtrl`);

--
-- Indexes for table `koran`
--
ALTER TABLE `koran`
  ADD PRIMARY KEY (`id_koran`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id_pkt`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_pjl`);

--
-- Indexes for table `pinjaman`
--
ALTER TABLE `pinjaman`
  ADD PRIMARY KEY (`id_pjm`);

--
-- Indexes for table `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id_price`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_prd`);

--
-- Indexes for table `rawmaterial`
--
ALTER TABLE `rawmaterial`
  ADD PRIMARY KEY (`id_raw`);

--
-- Indexes for table `saldo_akhir`
--
ALTER TABLE `saldo_akhir`
  ADD PRIMARY KEY (`id_sa`);

--
-- Indexes for table `sewa`
--
ALTER TABLE `sewa`
  ADD PRIMARY KEY (`id_sewa`);

--
-- Indexes for table `system_menu`
--
ALTER TABLE `system_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_module`
--
ALTER TABLE `system_module`
  ADD PRIMARY KEY (`id_pm`);

--
-- Indexes for table `system_user`
--
ALTER TABLE `system_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `system_user_level`
--
ALTER TABLE `system_user_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `system_user_rights`
--
ALTER TABLE `system_user_rights`
  ADD PRIMARY KEY (`id_up`);

--
-- Indexes for table `system_user_role`
--
ALTER TABLE `system_user_role`
  ADD PRIMARY KEY (`id_ur`);

--
-- Indexes for table `tabungan`
--
ALTER TABLE `tabungan`
  ADD PRIMARY KEY (`id_tab`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id_team`);

--
-- Indexes for table `team_type`
--
ALTER TABLE `team_type`
  ADD PRIMARY KEY (`id_tt`);

--
-- Indexes for table `termin`
--
ALTER TABLE `termin`
  ADD PRIMARY KEY (`id_trm`);

--
-- Indexes for table `transportation_cost`
--
ALTER TABLE `transportation_cost`
  ADD PRIMARY KEY (`id_tc`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beli_material`
--
ALTER TABLE `beli_material`
  MODIFY `id_bMtrl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id_cty` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `demand`
--
ALTER TABLE `demand`
  MODIFY `id_dmd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `denda`
--
ALTER TABLE `denda`
  MODIFY `id_denda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detail_jmtrl`
--
ALTER TABLE `detail_jmtrl`
  MODIFY `id_dtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `iklan`
--
ALTER TABLE `iklan`
  MODIFY `id_pIklan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `iklan_type`
--
ALTER TABLE `iklan_type`
  MODIFY `id_iklan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jual_material`
--
ALTER TABLE `jual_material`
  MODIFY `id_jMtrl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `koran`
--
ALTER TABLE `koran`
  MODIFY `id_koran` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id_pkt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id_pjl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pinjaman`
--
ALTER TABLE `pinjaman`
  MODIFY `id_pjm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `price`
--
ALTER TABLE `price`
  MODIFY `id_price` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id_prd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rawmaterial`
--
ALTER TABLE `rawmaterial`
  MODIFY `id_raw` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `saldo_akhir`
--
ALTER TABLE `saldo_akhir`
  MODIFY `id_sa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sewa`
--
ALTER TABLE `sewa`
  MODIFY `id_sewa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `system_menu`
--
ALTER TABLE `system_menu`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `system_module`
--
ALTER TABLE `system_module`
  MODIFY `id_pm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `system_user`
--
ALTER TABLE `system_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_user_level`
--
ALTER TABLE `system_user_level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_user_rights`
--
ALTER TABLE `system_user_rights`
  MODIFY `id_up` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `system_user_role`
--
ALTER TABLE `system_user_role`
  MODIFY `id_ur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tabungan`
--
ALTER TABLE `tabungan`
  MODIFY `id_tab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id_team` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `team_type`
--
ALTER TABLE `team_type`
  MODIFY `id_tt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `termin`
--
ALTER TABLE `termin`
  MODIFY `id_trm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transportation_cost`
--
ALTER TABLE `transportation_cost`
  MODIFY `id_tc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
