<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display extends CI_Controller {


	function __construct(){
        parent::__construct();
		$this->load->helper(array('form', 'url','email'));
		$this->load->model('model_cpanel');
		$this->load->model('model_general');
		$this->load->model('model_display');
		$this->load->model('model_database', 'dbs', TRUE);
    }

    public function demand (){
    	$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$data['title']='Demand';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['city']=$this->dbs->dmdCities()->result();
		$data['content']='display/demand';	
		$this->load->view('xrossbone/index',$data);
	}

	public function demandtable (){
		$this->load->view('xrossbone/demandtable');
	}

	public function rank (){
    	$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$data['title']='Demand';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['manuf']=$this->dbs->teamManuf()->result();
		$data['outsrc']=$this->dbs->teamOutsrc()->result();
		$data['log']=$this->dbs->teamLog()->result();
		$data['content']='display/rank';	
		$this->load->view('xrossbone/index',$data);
	}

	public function ranktable (){
		$this->load->view('xrossbone/ranktable');
	}


}
