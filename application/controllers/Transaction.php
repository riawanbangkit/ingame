<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {


	function __construct(){
        parent::__construct();
		$this->load->helper(array('form', 'url','email'));
		$this->load->model('model_cpanel');
		$this->load->model('model_general');
		$this->load->model('model_transaction');
		$this->load->model('model_database', 'dbs', TRUE);
    }

    public function penjualan (){
		$data['title']='Penjualan';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->teamManuf()->result();
		$data['dmd']=$this->dbs->dmdCities()->result();
		$data['sply']=$this->dbs->splyCities()->result();
		$data['content']='transaction/penjualan';	
		$this->load->view('xrossbone/index',$data);
	}
	
	function inputPenjualan(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$team = $this->input->post('team');
		$reg = $this->input->post('reg');
		$prm = $this->input->post('prm');
		$ctyfr = $this->input->post('ctyfr');
		$ctyto = $this->input->post('ctyto');
		$tc = $this->dbs->tc_frto($ctyfr,$ctyto,$trm)->row();
		$reg_p = $this->dbs->regPrice($ctyto,$trm)->row();
		$prm_p = $this->dbs->prmPrice($ctyto,$trm)->row();
		$dat= array(
			'id_team' =>  $team,
			'qty_reg' =>  $reg,
			'qty_prm' =>  $prm,
			'cty_to' =>  $ctyto,
			'id_tc' =>  $tc->id_tc,
			'id_trm' =>  $trm,
			'reg_total' =>  $reg*$reg_p->price,
			'prm_total' =>  $prm*$prm_p->price,
			'tc_total' =>  $tc->cost,
		);
		$this->model_transaction->inputPenjualan($dat);
		$id_pjl = $this->db->insert_id();
		$pjl = $this->dbs->getPenjualan($id_pjl)->row();
		$saldo = $this->dbs->getSaldoTeam($team)->row();
		$dat2= array(
			'hold' =>  $saldo->hold+$pjl->reg_total+$pjl->prm_total-$pjl->tc_total,
		);
		$this->model_transaction->updateSaldo($dat2,$team);
		$demandPrm = $this->dbs->getDemandPrm($ctyto,$trm)->row();
		$dat3= array(
			'demand' =>  $demandPrm->demand-$prm,
		);
		$this->model_transaction->updateDemandPrm($dat3,$ctyto,$trm);
		$demandReg = $this->dbs->getDemandReg($ctyto,$trm)->row();
		$dat4= array(
			'demand' =>  $demandReg->demand-$reg,
		);
		$this->model_transaction->updateDemandReg($dat4,$ctyto,$trm);
		redirect('transaction/penjualan','refresh');
	}

	public function koran (){
		$data['title']='Koran';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->teamManuf()->result();
		$data['content']='transaction/koran';	
		$this->load->view('xrossbone/index',$data);
	}


	function beliKoran(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$team = $this->input->post('team');
		$qty = $this->input->post('qty');
		$dat= array(
			'id_team' =>  $team,
			'qty_koran' =>  $qty,
			'total_cost' =>  $qty*500000,
			'id_trm' =>  $trm,
		);
		$this->model_transaction->inputKoran($dat);
		$id_koran = $this->db->insert_id();
		$koran = $this->dbs->getKoran($id_koran)->row();
		$saldo = $this->dbs->getSaldoTeam($team)->row();
		$dat2= array(
			'hold' =>  $saldo->hold-$koran->total_cost,
		);
		$this->model_transaction->updateSaldo($dat2,$team);
		redirect('transaction/koran','refresh');
	}

	public function iklan (){
		$data['title']='Iklan';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->teamLogOutsrc()->result();
		$data['type']=$this->dbs->allIklan()->result();
		$data['content']='transaction/iklan';	
		$this->load->view('xrossbone/index',$data);
	}

	function postIklan(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$team = $this->input->post('team');
		$iklan = $this->input->post('iklan');
		$durasi = $this->input->post('durasi');
		$cost = $this->dbs->getIklanType($iklan)->row();
		$dat= array(
			'id_team' =>  $team,
			'id_iklan' =>  $iklan,
			'durasi' =>  $durasi,
			'total_cost' =>  $durasi*$cost->iklan_price,
			'id_trm' =>  $trm,
		);
		$this->model_transaction->inputIklan($dat);
		$id_pIklan = $this->db->insert_id();
		$pIklan = $this->dbs->getPIklan($id_pIklan)->row();
		$saldo = $this->dbs->getSaldoTeam($team)->row();
		$dat2= array(
			'hold' =>  $saldo->hold-$pIklan->total_cost,
		);
		$this->model_transaction->updateSaldo($dat2,$team);
		redirect('transaction/iklan','refresh');
	}

	public function denda (){
		$data['title']='Denda';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->allTeam()->result();
		$data['content']='transaction/denda';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputDenda(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$team = $this->input->post('team');
		$denda = $this->input->post('denda');
		$dat= array(
			'id_team' =>  $team,
			'denda' =>  $denda,
			'id_trm' =>  $trm,
		);
		$this->model_transaction->inputDenda($dat);
		$id_denda = $this->db->insert_id();
		$denda = $this->dbs->getDenda($id_denda)->row();
		$saldo = $this->dbs->getSaldoTeam($team)->row();
		$dat2= array(
			'hold' =>  $saldo->hold-$denda->denda,
		);
		$this->model_transaction->updateSaldo($dat2,$team);
		redirect('transaction/denda','refresh');
	}

	public function beliMtrl (){
		$data['title']='Pembelian Material';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->teamManuf()->result();
		$data['paket']=$this->dbs->allPaket()->result();
		$data['content']='transaction/belimaterial';	
		$this->load->view('xrossbone/index',$data);
	}

	function pembelianMaterial(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$team = $this->input->post('team');
		$pkt = $this->input->post('pkt');
		$jml = $this->input->post('jml');
		$cost = $this->dbs->getPaket($pkt)->row();
		$dat= array(
			'id_team' =>  $team,
			'id_pkt' =>  $pkt,
			'jumlah' =>  $jml,
			'total_cost' =>  $jml*$cost->paket_price,
			'id_trm' =>  $trm,
		);
		$this->model_transaction->inputPembelianMtrl($dat);
		$id_bMtrl = $this->db->insert_id();
		$bMtrl = $this->dbs->getBMtrl($id_bMtrl)->row();
		$saldo = $this->dbs->getSaldoTeam($team)->row();
		$dat2= array(
			'hold' =>  $saldo->hold-$bMtrl->total_cost,
		);
		$this->model_transaction->updateSaldo($dat2,$team);
		redirect('transaction/beliMtrl','refresh');
	}

	public function jualMtrl (){
		$data['title']='Penjualan Material';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->teamManuf()->result();
		$data['raw']=$this->dbs->allRawmat()->result();
		$data['content']='transaction/jualmaterial';	
		$this->load->view('xrossbone/index',$data);
	}

	function penjualanMaterial(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$team = $this->input->post('team');
		$dat1= array(
			'id_team' =>  $team,
			'total_cost' =>  0,
			'id_trm' =>  $trm,
		);
		$this->model_transaction->inputPenjualanMtrl($dat1);
		$id_jMtrl = $this->db->insert_id();
		$i = 1;
		foreach ($this->input->post('raw') as $r) {
				$id_raw=$this->input->post('idr'.$i);
				$rawmat=$this->dbs->getRawmat($id_raw)->row();
				$dat= array(
				'id_jMtrl' =>  $id_jMtrl,
				'id_team' =>  $team,
				'id_raw' =>  $this->input->post('idr'.$i),
				'raw' =>  $r,
				'jPrice' =>  $r*$rawmat->raw_price,
				'id_trm' =>  $trm,
			);
			$this->model_transaction->inputDetailPenjualanMtrl($dat);
			$i++;
		}
		$sum = $this->dbs->sumDetail($id_jMtrl)->row();
		$tc = $this->dbs->getJualMaterial($id_jMtrl)->row();
		$dat2= array(
			'total_cost' =>  $tc->total_cost+$sum->cum,
		);
		$this->model_transaction->updateJualMaterial($dat2,$id_jMtrl);
		$saldo = $this->dbs->getSaldoTeam($team)->row();
		$dat3= array(
			'hold' =>  $saldo->hold+$sum->cum,
		);
		$this->model_transaction->updateSaldo($dat3,$team);
		redirect('transaction/jualMtrl','refresh');
	}

	public function tabungan (){
		$data['title']='Tabungan';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->allteam()->result();
		$data['content']='transaction/tabungan';	
		$this->load->view('xrossbone/index',$data);
	}

	function menabung(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$team = $this->input->post('team');
		$tt=$this->dbs->getTeam($team)->row();
		$tab = $this->input->post('tab');
		if($tt->id_tt = 1){
			if($tab > 25000000 && $tab <= 50000000){
                $bunga=0.05*$tab;} 
                else if ($tab > 50000000 && $tab <= 70000000){
                $bunga=0.1*$tab;}
                else if ($tab > 70000000){
                $bunga=0.15*$tab;}
                else if ($tab <= 25000000){
                $bunga=0*$tab;}
			}
		if ($tt->id_tt = 2 || $tt->id_tt = 3){
			if($tab > 2000000 && $tab <= 3000000){
                $bunga=0.05*$tab;} 
                else if ($tab > 3000000 && $tab <= 4000000){
                $bunga=0.1*$tab;}
                else if ($tab > 4000000){
                $bunga=0.15*$tab;}
                else if ($tab <= 2000000){
                $bunga=0*$tab;}
		}
		$dat= array(
			'id_team' =>  $team,
			'tab_type' =>  1,
			'tabungan' =>  $tab,
			'id_trm' =>  $trm,
		);
		$this->model_transaction->menabung($dat);
		$id_tab = $this->db->insert_id();
		$tab2 = $this->dbs->getTabungan($id_tab)->row();
		$saldo = $this->dbs->getSaldoTeam($team)->row();
		$dat2= array(
			'simpanan' =>  $saldo->simpanan+$tab2->tabungan+$bunga,
			'hold' =>  $saldo->hold-$tab2->tabungan,
		);
		$this->model_transaction->updateSaldo($dat2,$team);
		redirect('transaction/tabungan','refresh');
	}

	public function pinjaman (){
		$data['title']='Pinjaman';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->allteam()->result();
		$data['content']='transaction/pinjaman';	
		$this->load->view('xrossbone/index',$data);
	}

	function pinjamBank(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$team = $this->input->post('team');
		$pjm = $this->input->post('pjm');
		$dat= array(
			'id_team' =>  $team,
			'pinjaman' =>  $pjm,
			'id_trm' =>  $trm,
			'status_pjm' =>  1,
		);
		$this->model_transaction->pinjam($dat);
		$id_pjm = $this->db->insert_id();
		$pjm = $this->dbs->getPinjaman($id_pjm)->row();
		$saldo = $this->dbs->getSaldoTeam($team)->row();
		$dat2= array(
			'pinjaman' =>  $saldo->pinjaman+$pjm->pinjaman,
			'hold' =>  $saldo->hold+$pjm->pinjaman,
		);
		$this->model_transaction->updateSaldo($dat2,$team);
		redirect('transaction/pinjaman','refresh');
	}

	public function tarik (){
		$data['title']='Penarikan';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->allteam()->result();
		$data['content']='transaction/tarik';	
		$this->load->view('xrossbone/index',$data);
	}

	function penarikan(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$team = $this->input->post('team');
		$tab = $this->input->post('tab');
		$dat= array(
			'id_team' =>  $team,
			'tab_type' =>  2,
			'tabungan' =>  $tab,
			'id_trm' =>  $trm,
		);
		$this->model_transaction->menabung($dat);
		$id_tab = $this->db->insert_id();
		$tab = $this->dbs->getTabungan($id_tab)->row();
		$saldo = $this->dbs->getSaldoTeam($team)->row();
		$dat2= array(
			'simpanan' =>  $saldo->simpanan-$tab->tabungan,
			'hold' =>  $saldo->hold+$tab->tabungan,
		);
		$this->model_transaction->updateSaldo($dat2,$team);
		redirect('transaction/tarik','refresh');
	}

	public function bayarPinjaman (){
		$data['title']='Bayar Pinjaman';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['team']=$this->dbs->allteam()->result();
		$data['content']='transaction/bayarPinjaman';	
		$this->load->view('xrossbone/index',$data);
	}

	public function detailPjm(){
		$team = $this->input->post('team');
		$data['title']='Detail Pinjaman';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['pjm']=$this->dbs->getPjmTeam($team)->result();
		$data['content']='transaction/detailPinjaman';	
		$this->load->view('xrossbone/index',$data);
	}

	function pelunasan($id_pjm){
		$pjm = $this->dbs->getPinjaman($id_pjm)->row();
		$dat= array(
			'status_pjm' =>  2,
		);
		$this->model_transaction->pelunasan($dat,$id_pjm);
		$saldo = $this->dbs->getSaldoTeam($pjm->id_team)->row();
		$dat2= array(
			'pinjaman' =>  0,
			'hold' =>  $saldo->hold-($pjm->pinjaman+($pjm->pinjaman*(2.5/100))),
		);
		$this->model_transaction->updateSaldo($dat2,$pjm->id_team);
		redirect('main/dashboard','refresh');
	}

	public function sewaOutsource (){
		$data['title']='Outsource';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['manuf']=$this->dbs->teamManuf()->result();
		$data['outsrc']=$this->dbs->teamOutsrc()->result();
		$data['content']='transaction/swoutsrc';	
		$this->load->view('xrossbone/index',$data);
	}

	public function sewaLogistik (){
		$data['title']='Outsource';
		$data['trmact']=$this->dbs->trmact()->row();
		$data['manuf']=$this->dbs->teamManuf()->result();
		$data['log']=$this->dbs->teamLog()->result();
		$data['content']='transaction/swlog';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputSewa(){
		$termin = $this->dbs->trmact()->row();
		$trm = $termin->termin;
		$customer = $this->input->post('manuf');
		$provider = $this->input->post('prov');
		$cost = $this->input->post('cost');
		$dat= array(
			'customer' =>  $customer,
			'provider' =>  $provider,
			'cost' =>  $cost,
			'id_trm' =>  $trm,
		);
		$this->model_transaction->inputSewa($dat);
		$id_sewa = $this->db->insert_id();
		$sewa = $this->dbs->getSewa($id_sewa)->row();
		$saldo_cust = $this->dbs->getSaldoTeam($customer)->row();
		$dat2= array(
			'hold' =>  $saldo_cust->hold-$sewa->cost,
		);
		$this->model_transaction->updateSaldo($dat2,$customer);
		$saldo_prov = $this->dbs->getSaldoTeam($provider)->row();
		$dat3= array(
			'hold' =>  $saldo_prov->hold+$sewa->cost,
		);
		$this->model_transaction->updateSaldo($dat3,$provider);
		redirect('main/dashboard','refresh');
	}


}
