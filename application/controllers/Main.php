<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
        parent::__construct();
		$this->load->helper(array('form', 'url','email'));
		$this->load->model('model_cpanel');
		$this->load->model('model_general');
		$this->load->model('model_database', 'dbs', TRUE);
    }
	
	public function index()
	{
			if($_POST==NULL){
				$data['page_title'] = "Login | INGAME";
				$data['desc'] = "";
				$this->load->view('system_login/login', $data);
			}
			else{
					$user = $this->input->post('textUsername');
					$pass = $this->input->post('textPassword');
					$pass5=md5($pass);
					$q= $this->model_cpanel->checkUserLogin($user,$pass5)->row();
					if ($q->password == $pass5){
						$session_data = array(
							'id_user' => $q->id_user,
							'username' => $q->username,
							'nama' => $q->display_name,
							'lv' => $q->level,
							'lvname' => $q->level_name,
							'display' => $q->display_path,
							'is_login' => TRUE
						);
						if($q->level==1 || $q->level==2){
							$this->session->set_userdata($session_data);
							redirect('main/dashboard');
						}
						}else{
							redirect('main');
						}
				
			}
		
	}
	
	public function logout() {
		error_reporting(0);
        $data = array
            (
            'id_user' => 0,
			'username' => 0,
			'nama' => 0,
			'level' => 0,
			'display' => 0,
			'is_login' => FALSE
        );
        $this->session->sess_destroy();
        $this->session->unset_userdata($data);
        redirect('main');
		exit();
    }
	
	public function changeRole($iduser,$idlvl){
		$data = array(
               'level' => $idlvl
            );
		$this->model_general->requestChangeRole($iduser,$data);
		$q= $this->model_cpanel->getUser($iduser)->row();
		$session_data = array(
							'id_user' => $q->id_user,
							'username' => $q->username,
							'nama' => $q->display_name,
							'lv' => $q->level,
							'lvname' => $q->level_name,
							'display' => $q->display_path,
							'is_login' => TRUE
						);
		$this->session->set_userdata($session_data);
		redirect('main/dashboard', 'refresh');
	}
	
	public function dashboard (){
		$lv=$this->session->userdata('lv');
		switch($lv){
			case 1://Super Admin
				$data['title']='Dashboard';
				//$data['or']=$this->dbs->allOprec()->result();
				$data['saldo']=$this->dbs->allSaldo()->result();
				$data['trmact']=$this->dbs->trmact()->row();	
				$data['content']='dashboard/index';
				break;
			case 2://Data Entry
				$data['title']='Dashboard';
				$data['saldo']=$this->dbs->allSaldo()->result();
				$data['trmact']=$this->dbs->trmact()->row();
				$s=$this->session->userdata('username');
				$data['content']='dashboard/index';
				break;
			default:
				$data['title']='Dashboard';
				$data['or']=$this->dbs->allOprec()->result();	
				$data['content']='admin/index';	
		}	
		$this->load->view('xrossbone/index',$data);
	}
	
	
	public function sendNow(){
		$email='rayindasoesanto91@gmail.com';
		$htmlContent = '<h2>Thank You For Registration</h2>';
		$htmlContent .= '<p>Your Registration is complete with following details:<br>Username: '.$kode.'<br> Password: '.$pass.'.</p><br>You may now apply for laboratory assistant position.';
		$htmlContent .= '<br><p>Thank You.</p>';
		sendEmail($email,$htmlContent);
	}
	
	public function error(){
		$this->load->view('system_login/error');
	}
	public function success(){
		$this->load->view('system_login/success');
	}
}
