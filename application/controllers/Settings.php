<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {


	function __construct(){
        parent::__construct();
		$this->load->helper(array('form', 'url','email'));
		$this->load->model('model_cpanel');
		$this->load->model('model_general');
		$this->load->model('model_settings');
		$this->load->model('model_database', 'dbs', TRUE);
    }
	
    public function termin (){
		$data['title']='Termin';
		$data['trm']=$this->dbs->allTermin()->result();	
		$data['content']='settings/termin';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputTermin(){
		$name = $this->input->post('termin');
		$dat= array(
			'termin' =>  $name,
			'status_trm' =>  '0',
		);
		$this->model_settings->inputTermin($dat);
		redirect('settings/termin','refresh');
	}

	function activateTermin(){
		$id= $this->input->post('termact');
		$dat= array(
			'status_trm' =>  '1',
		);
		$dat2= array(
			'status_trm' =>  '0',
		);
		$this->model_settings->deactivateTermin($dat2);
		$this->model_settings->activateTermin($dat,$id);
		redirect('settings/termin','refresh');
	}

	function deleteTermin($id){
		$this->model_settings->deleteTermin($id);
		redirect('settings/termin','refresh');
	}

	public function cities (){
		$data['title']='Cities';
		$data['city']=$this->dbs->allCities()->result();	
		$data['content']='settings/cities';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputCity(){
		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$dat= array(
			'city_name' =>  $name,
			'type' =>  $type,
		);
		$this->model_settings->inputCity($dat);
		redirect('settings/cities','refresh');
	}

	function deleteCity($id){
		$this->model_settings->deleteCity($id);
		redirect('settings/cities','refresh');
	}

	public function product (){
		$data['title']='Products';
		$data['prd']=$this->dbs->allProduct()->result();	
		$data['content']='settings/product';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputProduct(){
		$name = $this->input->post('name');
		$desc = $this->input->post('desc');
		$dat= array(
			'product_name' =>  $name,
			'product_desc' =>  $desc,
		);
		$this->model_settings->inputProduct($dat);
		redirect('settings/product','refresh');
	}

	function deleteProduct($id){
		$this->model_settings->deleteProduct($id);
		redirect('settings/product','refresh');
	}

	public function rawmaterial (){
		$data['title']='Raw Material';
		$data['raw']=$this->dbs->allRawmat()->result();	
		$data['content']='settings/rawmaterial';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputRawmaterial(){
		$name = $this->input->post('name');
		$price = $this->input->post('price');
		$dat= array(
			'raw_name' =>  $name,
			'raw_price' =>  $price,
		);
		$this->model_settings->inputRawmat($dat);
		redirect('settings/rawmaterial','refresh');
	}

	function deleteRawmaterial($id){
		$this->model_settings->deleteRawmat($id);
		redirect('settings/rawmaterial','refresh');
	}

	public function paket (){
		$data['title']='Paket Raw Material';
		$data['pkt']=$this->dbs->allPaket()->result();	
		$data['content']='settings/paket';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputPaket(){
		$name = $this->input->post('paket');
		$price = $this->input->post('price');
		$dat= array(
			'paket_name' =>  $name,
			'paket_price' =>  $price,
		);
		$this->model_settings->inputPaket($dat);
		redirect('settings/paket','refresh');
	}

	function deletePaket($id){
		$this->model_settings->deletePaket($id);
		redirect('settings/paket','refresh');
	}

	public function demand (){
		$data['title']='Demands';
		$data['trm']=$this->dbs->allTermin()->result();
		$data['prd']=$this->dbs->allProduct()->result();	
		$data['city']=$this->dbs->dmdCities()->result();		
		$data['dmd']=$this->dbs->allDemand()->result();
		$data['content']='settings/demand';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputDemand(){
		$trm = $this->input->post('trm');
		$prd = $this->input->post('prd');
		$cty = $this->input->post('cty');
		$dmd = $this->input->post('demand');
		$dat= array(
			'id_trm' =>  $trm,
			'id_prd' =>  $prd,
			'id_cty' =>  $cty,
			'demand' =>  $dmd,
		);
		$this->model_settings->inputDemand($dat);
		redirect('settings/demand','refresh');
	}

	function deleteDemand($id){
		$this->model_settings->deleteDemand($id);
		redirect('settings/demand','refresh');
	}

	public function price (){
		$data['title']='Price';
		$data['trm']=$this->dbs->allTermin()->result();
		$data['prd']=$this->dbs->allProduct()->result();	
		$data['city']=$this->dbs->dmdCities()->result();		
		$data['price']=$this->dbs->allPrice()->result();
		$data['content']='settings/price';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputPrice(){
		$trm = $this->input->post('trm');
		$prd = $this->input->post('prd');
		$cty = $this->input->post('cty');
		$prc = $this->input->post('prc');
		$dat= array(
			'id_trm' =>  $trm,
			'id_prd' =>  $prd,
			'id_cty' =>  $cty,
			'price' =>  $prc,
		);
		$this->model_settings->inputPrice($dat);
		redirect('settings/price','refresh');
	}

	function deletePrice($id){
		$this->model_settings->deletePrice($id);
		redirect('settings/price','refresh');
	}

	public function transCost (){
		$data['title']='Transportation Cost';
		$data['trm']=$this->dbs->allTermin()->result();
		$data['fr']=$this->dbs->splyCities()->result();
		$data['to']=$this->dbs->dmdCities()->result();		
		$data['tc']=$this->dbs->allTransCost()->result();
		$data['content']='settings/transCost';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputTransCost(){
		$trm = $this->input->post('trm');
		$fr = $this->input->post('fr');
		$to = $this->input->post('to');
		$cost = $this->input->post('cost');
		$dat= array(
			'id_trm' =>  $trm,
			'fromcty' =>  $fr,
			'tocty' =>  $to,
			'cost' =>  $cost,
		);
		$this->model_settings->inputTransCost($dat);
		redirect('settings/transCost','refresh');
	}

	function deleteTransCost($id){
		$this->model_settings->deleteTransCost($id);
		redirect('settings/transCost','refresh');
	}

	public function team (){
		$data['title']='Team';	
		$data['tt']=$this->dbs->teamType()->result();
		$data['team']=$this->dbs->allTeam()->result();
		$data['content']='settings/team';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputTeam(){
		$name = $this->input->post('tname');
		$tt = $this->input->post('tt');
		$cptl = $this->input->post('cptl');
		$dat= array(
			'team_name' =>  $name,
			'id_tt' =>  $tt,
			'team_capital' =>  $cptl,
		);
		$this->model_settings->inputTeam($dat);
		$id_team = $this->db->insert_id();
		$dat2= array(
			'id_team' =>  $id_team,
			'hold' =>  $cptl,
			'simpanan' =>  '0',
			'pinjaman' =>  '0',
		);
		$this->model_settings->inputSaldo($dat2);
		redirect('settings/team','refresh');
	}

	function deleteTeam($id){
		$this->model_settings->deleteTeam($id);
		redirect('settings/team','refresh');
	}

	public function iklanType (){
		$data['title']='Setting Iklan';	
		$data['adv']=$this->dbs->allIklan()->result();
		$data['content']='settings/iklan';	
		$this->load->view('xrossbone/index',$data);
	}

	function inputIklanType(){
		$name = $this->input->post('adv');
		$price = $this->input->post('price');
		$dat= array(
			'iklan_type' =>  $name,
			'iklan_price' =>  $price,
		);
		$this->model_settings->inputIklanType($dat);
		redirect('settings/iklanType','refresh');
	}

	function deleteIklanType($id){
		$this->model_settings->deleteIklanType($id);
		redirect('settings/iklanType','refresh');
	}



}
