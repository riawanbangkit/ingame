<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cpanel extends CI_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('form', 'url','email'));
		$this->load->model('model_cpanel','cp',TRUE);
		$this->load->model('model_general');
		$this->load->model('model_database', 'dbs', TRUE);
    }
///user right//////////////////////////////////////////////////////////////////////////////////////////////	
	public function accessRights(){
		$data['lv']=$this->cp->allLevel()->result();
		$data['title']='Access Rights';
		$data['content']='cpanel/accessRights';			
		$this->load->view('xrossbone/index',$data);
	}
	public function addRights(){
		$data['level']=$this->cp->allLevel()->result();
		$data['title']='Access Rights';
		$data['content']='cpanel/addRights';						
		$this->load->view('xrossbone/index2',$data);
	}
	function detailRights($id){
		$data['l']=$this->cp->getLevel($id)->row();
		$data['r']=$this->cp->getRights($id)->result();
		$data['m']=$this->cp->allModule()->result();
		$data['title']='Access Rights';
		$data['content']='cpanel/detailRights';			
		$this->load->view('xrossbone/index2',$data);
	}
	function requestAccessRights(){
		$m = $this->input->post('m');
		$lv = $this->input->post('lv');
		$dat= array(
			'id_level' =>  $lv,
			'id_pm' =>  $m,
		);
		$this->cp->requestAccessRights($dat);
		redirect('cpanel/detailRights/'.$lv,'refresh');
	}
	function deleteRights($id,$lv){
		$this->cp->deleteRights($id);
		redirect('cpanel/detailRights/'.$lv,'refresh');
	}
///end of user right///////////////////////////////////////////////////////////////////////////////////////
///user right//////////////////////////////////////////////////////////////////////////////////////////////	
	public function account(){
		$data['acc']=$this->cp->allAccount()->result();
		$data['title']='User Account';
		$data['content']='cpanel/account';			
		$this->load->view('xrossbone/index',$data);
	}
	public function addAccount(){
		$data['level']=$this->cp->allLevel()->result();
		$data['title']='User Account';
		$data['content']='cpanel/addAccount';						
		$this->load->view('xrossbone/index2',$data);
		//$this->load->view('cpanel/debug');
	}
	public function editAccount($id){
		$data['ac']=$this->cp->getAccount($id)->row();
		$data['level']=$this->cp->allLevel()->result();
		$data['title']='User Account';
		$data['content']='cpanel/editAccount';						
		$this->load->view('xrossbone/index2',$data);
		//$this->load->view('cpanel/debug');
	}
	
	function deleteAccount($id){
		$this->cp->deleteAccount($id);
		redirect('cpanel/account','refresh');
	}
	
	function requestAccount(){
		$uname = $this->input->post('username');
		$pass = $this->input->post('pass');
		$cpass = $this->input->post('cpass');
		$dname = $this->input->post('dname');
		$email = $this->input->post('email');
		$lv = $this->input->post('lv');
		$dat= array(
			'username' =>  $uname,
			'password' =>  md5($pass),
			'display_name' =>  $dname,
			'email' =>  $email,
			'level' =>  $lv,
		);
		$this->cp->requestAccount($dat);
		redirect('cpanel/account','refresh');
	}
	function requestUpdateAccount(){
		$id = $this->input->post('id');
		$uname = $this->input->post('username');
		$pass = $this->input->post('pass');
		$rpass = $this->input->post('cpass');
		$dname = $this->input->post('dname');
		$email = $this->input->post('email');
		$lv = $this->input->post('lv');
		$dat= array(
			'username' =>  $uname,
			'password' =>  md5($pass),
			'display_name' =>  $dname,
			'email' =>  $email,
			'level' =>  $lv,
		);
		$this->cp->requestUpdateAccount($dat,$id);
		redirect('cpanel/account','refresh');
	}
///end of user right///////////////////////////////////////////////////////////////////////////////////////
///The Menu//////////////////////////////////////////////////////////////////////////////////////////////	
	public function menu(){
		$data['mn']=$this->cp->parentMenu()->result();
		$data['m']=$this->cp->allModule()->result();
		$data['title']='Menu';
		$data['content']='cpanel/menu';			
		$this->load->view('xrossbone/index2',$data);
	}
	function requestMenu(){
		$dat= array(
			'menu_name' =>  $this->input->post('mname'),
			'id_pm' =>  $this->input->post('m'),
			'parent_id' =>  '0',
			'url' =>  $this->input->post('url'),
			'icon' =>  $this->input->post('icon')
		);
		$this->cp->requestMenu($dat);
		redirect('cpanel/menu','refresh');
	}
	function editMenu($id){
		$data['mn']=$this->cp->getMenu($id)->row();
		$data['md']=$this->cp->allModule()->result();
		$data['title']='Menu';
		$data['content']='cpanel/editMenu';			
		$this->load->view('xrossbone/index2',$data);
	}
	function requestUpdateMenu(){
		$id=$this->input->post('id');
		$dat= array(
			'menu_name' =>  $this->input->post('mname'),
			'id_pm' =>  $this->input->post('m'),
			'parent_id' =>  '0',
			'url' =>  $this->input->post('url'),
			'icon' =>  $this->input->post('icon')
		);
		$this->cp->updateMenu($dat,$id);
		redirect('cpanel/menu','refresh');
	}
	function deleteMenu($id){
		$this->cp->deleteMenu($id);
		redirect('cpanel/menu','refresh');
	}
	function detailMenu($id){
		$data['p']=$this->cp->getMenu($id)->row();
		$data['id']=$id;
		$data['mn']=$this->cp->childMenu($id)->result();
		$data['title']='Menu';
		$data['content']='cpanel/detailMenu';			
		$this->load->view('xrossbone/index2',$data);
	}
	function requestChildMenu(){
		$id=$this->input->post('id');	
		$dat= array(
			'menu_name' =>  $this->input->post('mname'),
			'id_pm' =>  $this->input->post('m'),
			'parent_id' =>  $id,
			'menu_order' =>  $this->input->post('ord'),
			'url' =>  $this->input->post('url')
		);
		$this->cp->requestMenu($dat);
		redirect('cpanel/detailMenu/'.$id,'refresh');
	}
	function editChildMenu($id){
		$data['mn']=$this->cp->getMenu($id)->row();
		$data['md']=$this->cp->allModule()->result();
		$data['p']=$this->cp->parentMenu()->result();
		$data['title']='Menu';
		$data['content']='cpanel/editChildMenu';			
		$this->load->view('xrossbone/index2',$data);
	}
	function requestUpdateChildMenu(){
		$pid=$this->input->post('pid');	
		$id=$this->input->post('id');	
		$dat= array(
			'menu_name' =>  $this->input->post('mname'),
			'menu_order' =>  $this->input->post('ord'),
			'url' =>  $this->input->post('url')
		);
		$this->cp->updateMenu($dat,$id);
		redirect('cpanel/detailMenu/'.$pid,'refresh');
	}
	function deleteChildMenu($id,$pid){
		$this->cp->deleteMenu($id);
		redirect('cpanel/detailMenu/'.$pid,'refresh');
	}
///end of the menu///////////////////////////////////////////////////////////////////////////////////////	
}