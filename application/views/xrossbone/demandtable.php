<?php 
$trmact=$this->dbs->trmact()->row();
$city=$this->dbs->dmdCities()->result();
?>

<div class="card bg-white">
          <div class="card-header">
            <b>DEMAND - TERMIN <?= $trmact->termin ?></b>
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-4">City Name</th>
                  <th class="col-md-4">Reguler</th>
                  <th class="col-md-4">Premium</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($city as $t){
                  $reg = $this->dbs->getDemandReg($t->id_cty,$trmact->termin)->row();
                  $prm = $this->dbs->getDemandPrm($t->id_cty,$trmact->termin)->row();
                  $priceprm = $this->dbs->getPricePrm($t->id_cty,$trmact->termin)->row();
                  $pricereg = $this->dbs->getPriceReg($t->id_cty,$trmact->termin)->row();?>
                <tr>
                  <td><?=$t->city_name?></td>
                <?php if ($reg != NULL){ ?>
                  <td><?=$reg->demand?> (Rp <?= number_format($pricereg->price, 0, ',', '.')?>)</td>
                <?php } else { ?>
                  <td>0</td>
                <?php } ?>
                <?php if ($prm != NULL){ ?>
                  <td><?=$prm->demand?> (Rp <?= number_format($priceprm->price, 0, ',', '.')?>)</td>
                <?php } else { ?>
                  <td>0</td>
                <?php } ?>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
  </div>