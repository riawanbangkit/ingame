			
			<div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
					<?php
						$level=$this->session->userdata('lv');
						$menu = $this->model_general->getMenu($level)->result();
						foreach($menu as $m):
						$id = $m->id;
						$data = $this->model_general->getChildMenu($id);
						$x = $data->result();
						$s = $data->num_rows();
						if ($s != NULL){
					?>
				  <li><a><i class="<?=$m->icon?>"></i> <?=$m->menu_name?> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
						<?php foreach($x as $n): ?>
                      <li><a href="<?=base_url()?><?=$n->url?>"><?=$n->menu_name?></a></li>
					  <?php endforeach?>
                    </ul>
                  </li>
                 <?php } else { ?>
				  <li>
					  <a href="<?=base_url()?><?=$m->url?> ">
						  <i class="<?=$m->icon?>"></i> 
						  <?=$m->menu_name?> 
						  
					  </a>
                  </li>
				  <?php } endforeach ?>
                </ul>
              </div>