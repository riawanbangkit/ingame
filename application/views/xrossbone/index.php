<!doctype html>
<!--
Industrial Game } INGAME
Build For :  School of Industrial and System Engineering
Lead Programmer/Creator : Ray Soesanto | RYP
Junior Programmer : Bangkit Riawan
Date : Mei 2018
This Information System is build based on Xrossbone System by Ray Soesanto
This is part of intgr project
 ______                    ______                                                                   
(_____ \                  / _____)                                     _                            
 _____) ) _____  _   _   ( (____    ___   _____   ___  _____  ____   _| |_   ___                    
|  __  / (____ || | | |   \____ \  / _ \ | ___ | /___)(____ ||  _ \ (_   _) / _ \                   
| |  \ \ / ___ || |_| |   _____) )| |_| || ____||___ |/ ___ || | | |  | |_ | |_| |                  
|_|   |_|\_____| \__  |  (______/  \___/ |_____)(___/ \_____||_| |_|   \__) \___/                   
                (____/                                                                              
-->
<?php
$link_favicon = array(
	'href' => 'themes/fav.png',
	'rel' => 'shortcut icon',
	'type' => 'image/x-icon');
	
?>
<html class="no-js" lang="">	
<head>
  <meta charset="utf-8">
  <title>INGAME | <?=$title?></title>
  <?= link_tag($link_favicon);?>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- page stylesheets -->
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/selectize/dist/css/selectize.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/datatables/media/css/datatables.bootstrap.css">
  <!-- end page stylesheets -->
  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/styles/webfont.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/styles/climacons-font.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/styles/font-awesome.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/styles/card.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/styles/sli.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/styles/animate.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/styles/app.css">
  <link rel="stylesheet" href="<?=base_url()?>themes/admin/styles/app.skins.css">
  <!-- endbuild -->
    
</head>

<body class="page-loading">
  <!-- page loading spinner -->
  <div class="pageload">
    <div class="pageload-inner">
      <div class="sk-rotating-plane"></div>
    </div>
  </div>
  <!-- /page loading spinner -->
  <div class="app layout-fixed-header">
    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">
      <div class="brand">
        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-apps hidden-xs" data-toggle="quick-launch">
          <i class="icon-grid"></i>
        </a>
        <!-- /toggle small sidebar menu -->
        <!-- toggle offscreen menu -->
        <div class="toggle-offscreen">
          <a href="javascript:;" class="visible-xs hamburger-icon" data-toggle="offscreen" data-move="ltr">
            <span></span>
            <span></span>
            <span></span>
          </a>
        </div>
        <!-- /toggle offscreen menu -->
        <!-- logo -->
        <a class="brand-logo">
          <img src="<?=base_url()?>/themes/admin/images/logo.png" width="87">
        </a>
        <a href="#" class="small-menu-visible brand-logo">i.</a>
        <!-- /logo -->
      </div>
      <!--ul class="quick-launch-apps hide">
        <li>
          <a href="apps-gallery.html">
            <span class="app-icon bg-danger text-white">
            G
            </span>
            <span class="app-title">Gallery</span>
          </a>
        </li>
        <li>
          <a href="apps-messages.html">
            <span class="app-icon bg-success text-white">
            M
            </span>
            <span class="app-title">Messages</span>
          </a>
        </li>
        <li>
          <a href="apps-social.html">
            <span class="app-icon bg-primary text-white">
            S
            </span>
            <span class="app-title">Social</span>
          </a>
        </li>
        <li>
          <a href="apps-travel.html">
            <span class="app-icon bg-info text-white">
            T
            </span>
            <span class="app-title">Travel</span>
          </a>
        </li>
      </ul-->
      <!-- main navigation -->
      <nav role="navigation">
        <ul class="nav">
			<?php
			$level=$this->session->userdata('lv');
			$menu = $this->model_general->getMenu($level)->result();
			foreach($menu as $m):
				$id = $m->id;
				$data = $this->model_general->getChildMenu($id);
				$x = $data->result();
				$s = $data->num_rows();
				if ($s != NULL){
			?>
			<li>
				<a href="javascript:;">
				  <i class="<?=$m->icon?>"></i>
				  <span> <?=$m->menu_name?></span>
				</a>
				<ul class="sub-menu">
				<?php foreach($x as $n): ?>
					<li>
						<a href="<?=base_url()?><?=$n->url?>">
						<span><?=$n->menu_name?></span>
						</a>
					</li>
				<?php endforeach?>	
				</ul>
			</li>
			<?php } else { ?>
			<li>
				<a href="<?=base_url()?><?=$m->url?>">
					<i class="<?=$m->icon?>"></i>
					<span><?=$m->menu_name?></span>
				</a>
			</li>
			<?php } endforeach ?>
        </ul>
      </nav>
      <!-- /main navigation -->
    </div>
    <!-- /sidebar panel -->
    <!-- content panel -->
    <div class="main-panel">
      <!-- top header -->
      <div class="header navbar">
        <div class="brand visible-xs">
          <!-- toggle offscreen menu -->
          <div class="toggle-offscreen">
            <a href="javascript:;" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <!-- /toggle offscreen menu -->
          <!-- logo -->
          <a class="brand-logo">
            <span>ingram.</span>
          </a>
          <!-- /logo -->
        </div>
        <ul class="nav navbar-nav hidden-xs">
          <li>
            <a href="javascript:;" class="small-sidebar-toggle ripple" data-toggle="layout-small-menu">
              <i class="icon-toggle-sidebar"></i>
            </a>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right hidden-xs">
          <li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <span>User Role: <?=$this->session->userdata('lvname')?></span>
            </a>
           
          </li>

          <li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <img src="<?=base_url()?>themes/admin/images/avatar.jpg" class="header-avatar img-circle" alt="user" title="user">
              <span><?=$this->session->userdata('nama')?></span>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="javascript:;">Settings</a>
              </li>
              
              <li role="separator" class="divider"></li>
              <li>
                <a href="javascript:;">Help</a>
              </li>
              <li>
                <a href="<?=base_url()?>main/logout">Logout</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /top header -->
      <!-- main area -->
      <div class="main-content checkbo">
        <?php $this->load->view($content); ?>
      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->
    <!-- bottom footer -->
    <!--footer class="content-footer">
      <nav class="footer-right">
        <ul class="nav">
          <li>
            <a href="http://sie.telkomuniversity.ac.id">INGAME | &copy 2018 School of Industrial and System Engineering</a>
          </li>
          <li>
            <a href="javascript:;" class="scroll-up">
              <i class="fa fa-angle-up"></i>
            </a>
          </li>
        </ul>
      </nav>
    </footer-->
    <!-- /bottom footer -->
  </div>
  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="<?=base_url()?>themes/admin/scripts/helpers/modernizr.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/jquery/dist/jquery.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/fastclick/lib/fastclick.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="<?=base_url()?>themes/admin/scripts/helpers/smartresize.js"></script>
  <script src="<?=base_url()?>themes/admin/scripts/constants.js"></script>
  <script src="<?=base_url()?>themes/admin/scripts/main.js"></script>
  <!-- endbuild -->
  <!-- page scripts -->
  <script src="<?=base_url()?>themes/admin/vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/moment/min/moment.min.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/select2/dist/js/select2.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/multiselect/js/jquery.multi-select.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/datatables/media/js/jquery.dataTables.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="<?=base_url()?>themes/admin/vendor/scripts/forms/plugins.js"></script>
  <script src="<?=base_url()?>themes/admin/vendor/datatables/media/js/datatables.bootstrap.js"></script>
  <script type="text/javascript">
    $('.datatable').dataTable({
      
    });
  </script>
  <script type="text/javascript">
    var auto_refresh = setInterval(
    function ()
    {
    $('#load_table').load('<?=base_url()?>display/demandtable').fadeIn("slow");
    }, 100); // refresh every 100 milliseconds
  </script>
  <script type="text/javascript">
    var auto_refresh = setInterval(
    function ()
    {
    $('#load_rank').load('<?=base_url()?>display/ranktable').fadeIn("slow");
    }, 100); // refresh every 100 milliseconds
  </script>
  <!-- end initialize page scripts -->
</body>

</html>