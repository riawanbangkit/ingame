<?php 
$trmact=$this->dbs->trmact()->row();
$manuf=$this->dbs->rankManuf()->result();
$outsrc=$this->dbs->rankOutsrc()->result();
$log=$this->dbs->rankLog()->result();
?>

<div class="col-md-6">
<div class="card bg-white m-b">
          <div class="card-header">
            <b>MANUFAKTUR</b>
          </div>
          <div class="card-block p-a-0">
            <div class="table-responsive">
              <table class="table m-b-0">
                <thead>
                  <tr>
                    <th>Rank</th>
                    <th>Nama Team</th>
                    <th>Total Saldo</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($manuf as $t){ 
                       ?>
                  <tr>
                    <td><?= $i ?></td>
                    <td><?= $t->team_name ?></td>
                    <td><?= number_format($t->total, 0, ',', '.')?></td>
                  </tr>
                  <?php $i++;} ?> 
                </tbody>
              </table>
            </div>
          </div>
        </div>
  </div>

<div class="col-md-6">
<div class="card bg-white m-b">
          <div class="card-header">
            <b>OUTSOURCE</b>
          </div>
          <div class="card-block p-a-0">
            <div class="table-responsive">
              <table class="table m-b-0">
                <thead>
                  <tr>
                    <th>Rank</th>
                    <th>Nama Team</th>
                    <th>Total Saldo</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($outsrc as $u){ 
                      ?>
                  <tr>
                    <td><?= $i ?></td>
                    <td><?= $u->team_name ?></td>
                    <td><?= number_format($u->total, 0, ',', '.')?></td>
                  </tr>
                  <?php $i++;} ?> 
                </tbody>
              </table>
            </div>
          </div>
</div>
<div class="card bg-white m-b">
          <div class="card-header">
            <b>LOGISTIK</b>
          </div>
          <div class="card-block p-a-0">
            <div class="table-responsive">
              <table class="table m-b-0">
                <thead>
                  <tr>
                    <th>Rank</th>
                    <th>Nama Team</th>
                    <th>Total Saldo</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($log as $v){ 
                      ?>
                  <tr>
                    <td><?= $i ?></td>
                    <td><?= $v->team_name ?></td>
                    <td><?= number_format($v->total, 0, ',', '.')?></td>
                  </tr>
                  <?php $i++;} ?> 
                </tbody>
              </table>
            </div>
          </div>
</div>
</div>