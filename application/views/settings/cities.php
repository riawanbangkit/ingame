<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Add City
          </div>
          <div class="card-block">
             <?= form_open('settings/inputCity',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">City Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" placeholder="Insert City Name" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Type</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Termin" name="type" class="form-control" style="width: 100%;">
                      <option value="1">Supply</option>
                      <option value="2">Demand</option>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>


<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Cities
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-5">City Name</th>
                  <th class="col-md-5">City Type</th>
                  <th class="col-md-2">Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($city as $t){?>
                <tr>
                  <td><?=$t->city_name?></td>
                  <td>
                    <?php if ($t->type == 1){ ?>Supply
                    <?php }else { ?> Demand <?php } ?></td>
                  <td>
                    <a href="<?=base_url()?>settings/deleteCity/<?=$t->id_cty?>" onClick="return confirm('Are you sure?')">
                      <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                        <i class="fa fa-trash-o"></i>
                        <span>Delete</span>
                      </button>
                    </a>
                  </td>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>