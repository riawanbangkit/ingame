<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Add Product
          </div>
          <div class="card-block">
             <?= form_open('settings/inputProduct',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Product Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" placeholder="Insert Product Name" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Product Descriptions</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3" name="desc"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>


<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Product
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-5">Product Name</th>
                  <th class="col-md-5">Product Description</th>
                  <th class="col-md-2">Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($prd as $t){?>
                <tr>
                  <td><?=$t->product_name?></td>
                  <td><?=$t->product_desc?></td>
                  <td>
                    <a href="<?=base_url()?>settings/deleteProduct/<?=$t->id_prd?>" onClick="return confirm('Are you sure?')">
                      <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                        <i class="fa fa-trash-o"></i>
                        <span>Delete</span>
                      </button>
                    </a>
                  </td>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>