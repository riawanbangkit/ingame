<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);?>
<div class="col-md-8">
<div class="card bg-white">
          <div class="card-header">
            Add Termin
          </div>
          <div class="card-block">
             <?= form_open('settings/inputTermin',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Termin</label>
                <div class="col-sm-10">
                    <input type="text" name="termin" class="form-control" placeholder="Insert Termin" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>
<div class="col-md-4">
<div class="card bg-white">
          <div class="card-header">
            Activate termin
          </div>
           <div class="card-block">
             <?= form_open('settings/activateTermin',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Termin</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Termin" name="termact" class="form-control" style="width: 100%;">
                      <option value=""></option>
                      <?php
                        foreach($trm as $a){
                        ?>  
                        <option value="<?=$a->id_trm?>">Termin <?=$a->termin?></option>
                        <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Activate</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>

<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Termin
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-5">Termin</th>
                  <th class="col-md-5">Status</th>
                  <th class="col-md-2">Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($trm as $t){?>
                <tr>
                  <td>Termin <?=$t->termin?></td>
                  <td>
                    <?php if ($t->status_trm == 1){ ?>Active
                    <?php }else { ?> Non Active <?php } ?></td>
                  <td>
                    <a href="<?=base_url()?>settings/deleteTermin/<?=$t->id_trm?>" onClick="return confirm('Are you sure?')">
                      <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                        <i class="fa fa-trash-o"></i>
                        <span>Delete</span>
                      </button>
                    </a>
                  </td>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>