<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);
?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Input Transportation Cost
          </div>
          <div class="card-block">
             <?= form_open('settings/inputTeam',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Team Name</label>
                <div class="col-sm-10">
                    <input type="text" name="tname" class="form-control" placeholder="Insert Team Name" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Team Type</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Termin" name="tt" class="form-control" style="width: 100%;">
                        <?php
                          foreach($tt as $a){
                          ?>  
                          <option value="<?=$a->id_tt?>"><?=$a->team_type?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Team Capital</label>
                <div class="col-sm-10">
                    <input type="text" name="cptl" class="form-control" placeholder="Insert Team Capital" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>


<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Transportation Cost
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-2">Team Name</th>
                  <th class="col-md-2">Team type</th>
                  <th class="col-md-2">Capital</th>
                  <th class="col-md-2">Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($team as $t){?>
                <tr>
                  <td><?= $t->team_name ?></td>
                  <td><?= $t->team_type ?></td>
                  <td>Rp <?= number_format($t->team_capital, 0, ',', '.')?></td>
                  <td>
                    <a href="<?=base_url()?>settings/deleteTeam/<?=$t->id_team?>" onClick="return confirm('Are you sure?')">
                      <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                        <i class="fa fa-trash-o"></i>
                        <span>Delete</span>
                      </button>
                    </a>
                  </td>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>