<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);
?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Input Transportation Cost
          </div>
          <div class="card-block">
             <?= form_open('settings/inputTransCost',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Termin</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Termin" name="trm" class="form-control" style="width: 100%;">
                        <?php
                          foreach($trm as $a){
                          ?>  
                          <option value="<?=$a->id_trm?>">Termin <?=$a->termin?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">From</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Product" name="fr" class="form-control" style="width: 100%;">
                        <?php
                          foreach($fr as $b){
                          ?>  
                          <option value="<?=$b->id_cty?>"><?=$b->city_name?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">To</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Kota" name="to" class="form-control" style="width: 100%;">
                        <?php
                          foreach($to as $c){
                          ?>  
                          <option value="<?=$c->id_cty?>"><?=$c->city_name?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label">Cost</label>
                <div class="col-sm-10">
                    <input type="text" name="cost" class="form-control" placeholder="Insert Price" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>


<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Transportation Cost
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-2">From</th>
                  <th class="col-md-2">To</th>
                  <th class="col-md-2">Termin</th>
                  <th class="col-md-2">Cost</th>
                  <th class="col-md-2">Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($tc as $t){
                  $fr= $this->dbs->oneCities($t->fromcty)->row();
                  $to= $this->dbs->oneCities($t->tocty)->row();?>
                <tr>
                  <td><?= $fr->city_name ?></td>
                  <td><?= $to->city_name ?></td>
                  <td>Termin <?=$t->termin?></td>
                  <td>Rp <?= number_format($t->cost, 0, ',', '.')?></td>
                  <td>
                    <a href="<?=base_url()?>settings/deleteTransCost/<?=$t->id_tc?>" onClick="return confirm('Are you sure?')">
                      <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                        <i class="fa fa-trash-o"></i>
                        <span>Delete</span>
                      </button>
                    </a>
                  </td>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>