<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Add Raw Material
          </div>
          <div class="card-block">
             <?= form_open('settings/inputRawmaterial',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Material Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" placeholder="Insert Material Name" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Price</label>
                <div class="col-sm-10">
                    <input type="text" name="price" class="form-control" placeholder="Insert Price" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>


<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Raw Material
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-5">Material Name</th>
                  <th class="col-md-5">Material Price</th>
                  <th class="col-md-2">Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($raw as $t){?>
                <tr>
                  <td><?=$t->raw_name?></td>
                  <td>Rp <?= number_format($t->raw_price, 0, ',', '.')?></td>
                  <td>
                    <a href="<?=base_url()?>settings/deleteRawmaterial/<?=$t->id_raw?>" onClick="return confirm('Are you sure?')">
                      <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                        <i class="fa fa-trash-o"></i>
                        <span>Delete</span>
                      </button>
                    </a>
                  </td>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>