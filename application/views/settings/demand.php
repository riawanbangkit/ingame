<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Add New Demand
          </div>
          <div class="card-block">
             <?= form_open('settings/inputDemand',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Termin</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Termin" name="trm" class="form-control" style="width: 100%;">
                        <?php
                          foreach($trm as $a){
                          ?>  
                          <option value="<?=$a->id_trm?>">Termin <?=$a->termin?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Product</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Product" name="prd" class="form-control" style="width: 100%;">
                        <?php
                          foreach($prd as $b){
                          ?>  
                          <option value="<?=$b->id_prd?>"><?=$b->product_name?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">City</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Kota" name="cty" class="form-control" style="width: 100%;">
                        <?php
                          foreach($city as $c){
                          ?>  
                          <option value="<?=$c->id_cty?>"><?=$c->city_name?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Demand</label>
                <div class="col-sm-10">
                    <input type="text" name="demand" class="form-control" placeholder="Insert Demand" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>


<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Cities
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-5">City Name</th>
                  <th class="col-md-5">Termin</th>
                  <th class="col-md-5">Product</th>
                  <th class="col-md-5">Demand</th>
                  <th class="col-md-2">Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($dmd as $t){?>
                <tr>
                  <td><?=$t->city_name?></td>
                  <td>Termin <?=$t->termin?></td>
                  <td><?=$t->product_name?></td>
                  <td><?=$t->demand?></td>
                  <td>
                    <a href="<?=base_url()?>settings/deleteDemand/<?=$t->id_dmd?>" onClick="return confirm('Are you sure?')">
                      <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                        <i class="fa fa-trash-o"></i>
                        <span>Delete</span>
                      </button>
                    </a>
                  </td>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>