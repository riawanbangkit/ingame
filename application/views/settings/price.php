<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);
?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Input Price
          </div>
          <div class="card-block">
             <?= form_open('settings/inputPrice',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Product</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Product" name="prd" class="form-control" style="width: 100%;">
                        <?php
                          foreach($prd as $b){
                          ?>  
                          <option value="<?=$b->id_prd?>"><?=$b->product_name?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">City</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Kota" name="cty" class="form-control" style="width: 100%;">
                        <?php
                          foreach($city as $c){
                          ?>  
                          <option value="<?=$c->id_cty?>"><?=$c->city_name?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Termin</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Termin" name="trm" class="form-control" style="width: 100%;">
                        <?php
                          foreach($trm as $a){
                          ?>  
                          <option value="<?=$a->id_trm?>">Termin <?=$a->termin?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Price</label>
                <div class="col-sm-10">
                    <input type="text" name="prc" class="form-control" placeholder="Insert Price" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>


<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Cities
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-2">Product</th>
                  <th class="col-md-2">City Name</th>
                  <th class="col-md-2">Termin</th>
                  <th class="col-md-2">Price</th>
                  <th class="col-md-2">Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($price as $t){?>
                <tr>
                  <td><?=$t->product_name?></td>
                  <td><?=$t->city_name?></td>
                  <td>Termin <?=$t->termin?></td>
                  <td>Rp <?= number_format($t->price, 0, ',', '.')?></td>
                  <td>
                    <a href="<?=base_url()?>settings/deletePrice/<?=$t->id_price?>" onClick="return confirm('Are you sure?')">
                      <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                        <i class="fa fa-trash-o"></i>
                        <span>Delete</span>
                      </button>
                    </a>
                  </td>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>