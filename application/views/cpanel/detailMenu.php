<?php
	$formAttribute = array(
	'class'=>"form-horizontal",
	'role'=>"form"
);?>
	<div class="page-title">
		<div class="title"><?=$title?></div>
		<div class="sub-title">The Main Course of the System indeed</div>
	</div>
	<div class="card bg-white">
        <div class="card-header">
			Detail Menu for: <?=$p->menu_name?>
        </div>
        <div class="card-block">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-modal-sm">
					<i class="fa fa-plus"></i>
					Add Child Menu
				</button>
            <br><br>
            <table class="table table-bordered table-striped datatable m-b-0">
				<thead>
					<tr>
						<th>Menu</th>
						<th>URL</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($mn as $a){?>
					<tr>
						<td><?=$a->menu_name?></td>
						<td><?=$a->url?></td>
						<td>
							<a href="<?=base_url()?>cpanel/editChildMenu/<?=$a->id?>">
								<button type="button" class="btn btn-warning btn-sm btn-icon mr5">
								  <i class="fa fa-edit"></i>
								  <span>Edit</span>
								</button>
							</a>
							<a href="<?=base_url()?>cpanel/deleteChildMenu/<?=$a->id?>/<?=$a->parent_id?>" onClick="return confirm('Are you sure komrad?')">
								<button type="button" class="btn btn-danger btn-sm btn-icon mr5">
								  <i class="fa fa-trash-o"></i>
								  <span>Delete</span>
								</button>
							</a>
						</td>
					</tr>
				<?php } ?>	
				</tbody>
            </table>
        </div>
    </div>
	<div class="modal bs-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				  <h4 class="modal-title">Add Menu</h4>
				</div>
				<?= form_open('cpanel/requestChildMenu/',$formAttribute);?>
				<div class="modal-body">
				  <p>Add the menu please.</p>
					<div class="form-group">
						<label class="col-sm-2 control-label">Menu Name</label>
						<div class="col-sm-10">
							<input type="hidden" name="id" value="<?=$p->id?>">
							<input type="hidden" name="m" value="<?=$p->id_pm?>">
							<input type="text" name="mname" class="form-control" placeholder="Insert the name please" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">URL</label>
						<div class="col-sm-10">
							<input type="text" name="url" class="form-control" placeholder="Insert the url please" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Menu Order</label>
						<div class="col-sm-10">
							<input type="text" name="ord" class="form-control" placeholder="Please set the order of the menu" required>
						</div>
					</div>
				</div>
				<div class="modal-footer no-border">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  <button type="submit" class="btn btn-primary">Submit</button>
				</div>
				<?= form_close(); ?>
			</div>
		</div>
	</div>