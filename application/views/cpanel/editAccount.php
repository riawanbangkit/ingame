<?php
	$formAttribute = array(
	'class'=>"form-horizontal",
	'role'=>"form"
);?>
	<div class="page-title">
		<div class="title"><?=$title?></div>
        <div class="sub-title">Let's add new acccount</div>
    </div>
	<div class="card bg-white">
        <div class="card-header">
			Add Account
        </div>
        <div class="card-block">
            <div class="row m-a-0">
				<div class="col-lg-12">
					<?= form_open('cpanel/requestUpdateAccount/',$formAttribute);?>
						<div class="form-group">
							<label class="col-sm-2 control-label">User Right</label>
							<div class="col-sm-10">
							<select required data-placeholder="Choose The Right Access" name="lv" class="chosen" style="width: 100%;">
								<option value=""></option>
								<?php
									foreach($level as $a){
									?>	
									<option value="<?=$a->id_level?>"><?=$a->level_name?></option>
									<?php } ?>
							</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Username</label>
							<div class="col-sm-10">
							<input type="text" name="username" value="<?=$ac->username?>" class="form-control" placeholder="Insert username for the account" required>
							<input type="hidden" name="id" value="<?=$ac->id_user?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Password</label>
							<div class="col-sm-10">
							<input type="password" name="pass" minlength="8" max="8" class="form-control" placeholder="Insert the password for the account. Max 8 Characters" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Confirm Password</label>
							<div class="col-sm-10">
							<input type="password" name="cpass" minlength="8" max="8" class="form-control" placeholder="Confirm your password from above" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Display Name</label>
							<div class="col-sm-10">
							<input type="text" name="dname" value="<?=$ac->display_name?>" class="form-control" placeholder="Insert the name to be display" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Email</label>
							<div class="col-sm-10">
							<input type="text" name="email" value="<?=$ac->email?>" class="form-control" placeholder="Insert the Email" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"></label>
							<div class="col-sm-10">
							<button class="btn btn-primary">Submit</button>
							<button class="btn btn-default">Reset</button>
							</div>
						</div>
					<?= form_close(); ?>
				</div>
            </div>
        </div>
    </div>