<?php
	$formAttribute = array(
	'class'=>"form-horizontal",
	'role'=>"form"
);?>
	<div class="page-title">
		<div class="title"><?=$title?></div>
        <div class="sub-title">Let's update the menu</div>
    </div>
	<div class="card bg-white">
        <div class="card-header">
			Update Menu
        </div>
        <div class="card-block">
            <div class="row m-a-0">
				<div class="col-lg-12">
					<?= form_open('cpanel/requestUpdateMenu/',$formAttribute);?>
						<div class="form-group">
							<label class="col-sm-2 control-label">Module</label>
							<div class="col-sm-10">
							<select required data-placeholder="Choose The Right Access" name="m" class="chosen" style="width: 100%;">
								<option value=""></option>
								<?php
									foreach($md as $a){
									?>	
									<option value="<?=$a->id_pm?>"><?=$a->module_name?></option>
									<?php } ?>
							</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Menu Name</label>
							<div class="col-sm-10">
							<input type="text" name="mname" value="<?=$mn->menu_name?>" class="form-control" placeholder="Insert username for the account" required>
							<input type="hidden" name="id" value="<?=$mn->id?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">URL</label>
							<div class="col-sm-10">
							<input type="text" name="url" value="<?=$mn->url?>" class="form-control" placeholder="Insert the name to be display" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Icon</label>
							<div class="col-sm-10">
							<input type="text" name="icon" value="<?=$mn->icon?>" class="form-control" placeholder="Insert the Email" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"></label>
							<div class="col-sm-10">
							<button class="btn btn-primary">Submit</button>
							<button class="btn btn-default">Reset</button>
							</div>
						</div>
					<?= form_close(); ?>
				</div>
            </div>
        </div>
    </div>