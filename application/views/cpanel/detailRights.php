<?php
	$formAttribute = array(
	'class'=>"form-horizontal",
	'role'=>"form"
);?>
	<div class="page-title">
		<div class="title"><?=$title?></div>
		<div class="sub-title">Let's choose what is right for them</div>
	</div>
	<div class="card bg-white">
        <div class="card-header">
			Access Rights Detail for : <?=$l->level_name?>
        </div>
        <div class="card-block">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-modal-sm">
					<i class="fa fa-plus"></i>
					Add Module
				</button>
            <br><br>
			<table class="table table-bordered table-striped datatable m-b-0">
				<thead>
					<tr>
						<th>ID</th>
						<th>Module</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($r as $a){?>
					<tr>
						<td><?=$a->id_pm?></td>
						<td><?=$a->module_name?></td>
						<td>
							<a href="<?=base_url()?>cpanel/deleteRights/<?=$a->id_up?>/<?=$a->id_level?>" onClick="return confirm('Are you sure komrad?')">
								<button type="button" class="btn btn-danger btn-sm btn-icon mr5">
								  <i class="fa fa-trash-o"></i>
								  <span>Delete</span>
								</button>
							</a>
						</td>
					</tr>
				<?php } ?>	
				</tbody>
            </table>
        </div>
    </div>
	<div class="modal bs-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			  <h4 class="modal-title">Add Module</h4>
			</div>
			<?= form_open('cpanel/requestAccessRights/',$formAttribute);?>
			<div class="modal-body">
			  <p>Add Module for this user access rights please.</p>
				<div class="form-group">
					<label class="col-sm-2 control-label">User Right</label>
					<div class="col-sm-10">
						<select data-placeholder="Choose The Right Access" name="m" class="chosen form-control" style="width: 100%;">
							<option value=""></option>
							<?php
								foreach($m as $n){
							?>	
							<option value="<?=$n->id_pm?>"><?=$n->module_name?></option>
							<?php } ?>
						</select>
						<input type="hidden" name="lv" value="<?=$l->id_level?>">
					</div>
				</div>
			</div>
			<div class="modal-footer no-border">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  <button type="submit" class="btn btn-primary">Submit</button>
			</div>
			<?= form_close(); ?>
		</div>
    </div>
  </div>