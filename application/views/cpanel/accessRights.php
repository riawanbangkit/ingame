	<div class="page-title">
		<div class="title"><?=$title?></div>
		<div class="sub-title">Let's choose what is right for them</div>
	</div>
	<div class="card bg-white">
        <div class="card-header">
			Access Rights List
        </div>
        <div class="card-block">
			<table class="table table-bordered table-striped datatable m-b-0">
				<thead>
					<tr>
						<th>Level</th>
						<th>Level Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($lv as $a){?>
					<tr>
						<td><?=$a->id_level?></td>
						<td><?=$a->level_name?></td>
						<td>
							<a href="<?=base_url()?>cpanel/detailRights/<?=$a->id_level?>">
								<button type="button" class="btn btn-success btn-sm btn-icon mr5">
								  <i class="fa fa-search"></i>
								  <span>Detail</span>
								</button>
							</a>
						</td>
					</tr>
				<?php } ?>	
				</tbody>
            </table>
        </div>
    </div>