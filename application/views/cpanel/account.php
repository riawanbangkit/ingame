	<div class="page-title">
		<div class="title"><?=$title?></div>
		<div class="sub-title">So what about the account?</div>
	</div>
	<div class="card bg-white">
        <div class="card-header">
			Account List
        </div>
        <div class="card-block">
			<a href="<?=base_url()?>cpanel/addAccount">
				<button type="button" class="btn btn-success">
					<i class="fa fa-plus"></i>
					Add Account
				</button>
			</a>
            <br><br>
            <table class="table table-bordered table-striped datatable m-b-0">
				<thead>
					<tr>
						<th>Username</th>
						<th>Display Name</th>
						<th>User Right</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($acc as $a){?>
					<tr>
						<td><?=$a->username?></td>
						<td><?=$a->display_name?></td>
						<td><?=$a->level_name?></td>
						<td>
							<a>
								<button type="button" class="btn btn-success btn-sm btn-icon mr5">
								  <i class="fa fa-search"></i>
								  <span>Detail</span>
								</button>
							</a>
							<a href="<?=base_url()?>cpanel/editAccount/<?=$a->id_user?>" onClick="return confirm('Are you sure komrad?')">
								<button type="button" class="btn btn-warning btn-sm btn-icon mr5">
								  <i class="fa fa-edit"></i>
								  <span>Edit</span>
								</button>
							</a>
							<a href="<?=base_url()?>cpanel/deleteAccount/<?=$a->id_user?>" onClick="return confirm('Are you sure komrad?')">
								<button type="button" class="btn btn-danger btn-sm btn-icon mr5">
								  <i class="fa fa-trash-o"></i>
								  <span>Delete</span>
								</button>
							</a>
						</td>
					</tr>
				<?php } ?>	
				</tbody>
            </table>
        </div>
    </div>