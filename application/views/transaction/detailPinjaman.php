<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            Detail Pinjaman
          </div>
          <div class="card-block">
            <table class="table table-bordered table-striped datatable m-b-0">
              <thead>
                <tr>
                  <th class="col-md-3">Nama Team</th>
                  <th class="col-md-3">Jumlah Pinjaman</th>
                  <th class="col-md-3">Kewajiban Pembayaran</th>
                  <th class="col-md-3">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($pjm as $t){?>
                <tr>
                  <td><?=$t->team_name?></td>
                  <td>Rp <?= number_format($t->pinjaman, 0, ',', '.')?></td>
                  <td>Rp <?= number_format($t->pinjaman+($t->pinjaman*(2.5/100)), 0, ',', '.')?></td>
                  <?php if ($t->status_pjm == 1){ ?>
                  <td>
                    <a href="<?=base_url()?>transaction/pelunasan/<?=$t->id_pjm?>" onClick="return confirm('Are you sure?')">
                      <button type="button" class="btn btn-primary btn-sm btn-icon mr5">
                        <i class="fa fa-check-circle-o"></i>
                        <span>Pelunasan</span>
                      </button>
                    </a>
                  </td>
                <?php } else { ?>
                  <td>LUNAS</td>
                <?php } ?>
                </tr>
                <?php } ?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>