<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            PENARIKAN TABUNGAN - Active Termin: <?= $trmact->termin ?>
          </div>
          <div class="card-block">
             <?= form_open('transaction/penarikan',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Team</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Team" name="team" class="form-control" style="width: 100%;">
                        <?php
                          foreach($team as $a){
                          ?>  
                          <option value="<?=$a->id_team?>"><?=$a->team_name?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Jumlah Penarikan</label>
                <div class="col-sm-10">
                    <input type="text" name="tab" class="form-control" placeholder="Jumlah Penarikan" required>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>
