<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            PINJAMAN BANK - Active Termin: <?= $trmact->termin ?>
          </div>
          <div class="card-block">
             <?= form_open('transaction/pinjamBank',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Team</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Team" name="team" class="form-control" style="width: 100%;">
                        <?php
                          foreach($team as $a){
                          ?>  
                          <option value="<?=$a->id_team?>"><?=$a->team_name?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Jumlah Pinjaman</label>
                <div class="col-sm-10">
                    <input type="text" name="pjm" class="form-control" placeholder="Jumlah Pinjaman" required>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>
