<?php
  $formAttribute = array(
  'class'=>"form-horizontal",
  'role'=>"form"
);?>
<div class="col-md-12">
<div class="card bg-white">
          <div class="card-header">
            IKLAN - Active Termin: <?= $trmact->termin ?>
          </div>
          <div class="card-block">
             <?= form_open('transaction/postIklan',$formAttribute);?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Team</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Team" name="team" class="form-control" style="width: 100%;">
                        <?php
                          foreach($team as $a){
                          ?>  
                          <option value="<?=$a->id_team?>"><?=$a->team_name?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Iklan</label>
                <div class="col-sm-10">
                    <select data-placeholder="Pilih Iklan" name="iklan" class="form-control" style="width: 100%;">
                        <?php
                          foreach($type as $b){
                          ?>  
                          <option value="<?=$b->id_iklan?>"><?=$b->iklan_type?></option>
                          <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Durasi Iklan</label>
                <div class="col-sm-10">
                    <input type="text" name="durasi" class="form-control" placeholder="Durasi Iklan" required>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <button class="btn btn-primary">Submit</button>
              </div>
            </div>
          <?= form_close(); ?>
          </div>
        </div>
</div>
