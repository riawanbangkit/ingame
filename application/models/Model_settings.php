<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_settings extends CI_Model {

	function inputTermin($data){
		$this->db->insert('termin', $data);
	}

	function deactivateTermin($data) {
		$this->db->update('termin',$data,array('status_trm'=>'1'));
	}

	function activateTermin($data,$id) {
		$this->db->update('termin',$data,array('id_trm'=>$id));
	}

	function deleteTermin($id) {
		$this->db->delete('termin', array('id_trm'=>$id));
	}

	function inputCity($data){
		$this->db->insert('cities', $data);
	}

	function deleteCity($id) {
		$this->db->delete('cities', array('id_cty'=>$id));
	}

	function inputProduct($data){
		$this->db->insert('product', $data);
	}

	function deleteProduct($id) {
		$this->db->delete('product', array('id_prd'=>$id));
	}

	function inputRawmat($data){
		$this->db->insert('rawmaterial', $data);
	}

	function deleteRawmat($id) {
		$this->db->delete('rawmaterial', array('id_raw'=>$id));
	}

	function inputPaket($data){
		$this->db->insert('paket', $data);
	}

	function deletePaket($id) {
		$this->db->delete('paket', array('id_pkt'=>$id));
	}

	function inputIklanType($data){
		$this->db->insert('iklan_type', $data);
	}

	function deleteIklanType($id) {
		$this->db->delete('iklan_type', array('id_iklan'=>$id));
	}

	function inputDemand($data){
		$this->db->insert('demand', $data);
	}

	function deleteDemand($id) {
		$this->db->delete('demand', array('id_dmd'=>$id));
	}

	function inputPrice($data){
		$this->db->insert('price', $data);
	}

	function deletePrice($id) {
		$this->db->delete('price', array('id_price'=>$id));
	}

	function inputTransCost($data){
		$this->db->insert('transportation_cost', $data);
	}

	function deleteTransCost($id) {
		$this->db->delete('transportation_cost', array('id_tc'=>$id));
	}

	function inputTeam($data){
		$this->db->insert('team', $data);
	}

	function deleteTeam($id) {
		$this->db->delete('team', array('id_team'=>$id));
	}

	function inputSaldo($data){
		$this->db->insert('saldo_akhir', $data);
	}

}