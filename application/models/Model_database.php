<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_database extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	function allMahasiswa(){
		$this->db
		->from('mahasiswa');
        $query = $this->db->get();
		return $query;
	}
	function getMhs($cd){
		$this->db
		->from('mahasiswa')
		->like('nim',$cd);
		$query = $this->db->get();
		return $query;
	}
	function getMhs2($cd){
		$this->db
		->from('mahasiswa')
		->where('nim',$cd);
		$query = $this->db->get();
		return $query;
	}
	function requestEntryReg($data){
		$this->db->insert('candidate', $data);
	}
	function requestEntryAccount($data){
		$this->db->insert('system_user', $data);
	}
	function allLab(){
		$this->db
		->from('laboratory');
        $query = $this->db->get();
		return $query;
	}

	function allTermin(){
		$this->db
		->from('termin');
        $query = $this->db->get();
		return $query;
	}



	function allCities(){
		$this->db
		->from('cities');
        $query = $this->db->get();
		return $query;
	}

	function dmdCities(){
		$this->db
		->from('cities')
		->where('type','2');
        $query = $this->db->get();
		return $query;
	}

	function splyCities(){
		$this->db
		->from('cities')
		->where('type','1');
        $query = $this->db->get();
		return $query;
	}

	function trmact(){
		$this->db
		->from('termin')
		->where('status_trm','1');
        $query = $this->db->get();
		return $query;
	}

	function oneCities($id){
		$this->db
		->from('cities')
		->where('id_cty',$id);
        $query = $this->db->get();
		return $query;
	}

	function teamType(){
		$this->db
		->from('team_type');
        $query = $this->db->get();
		return $query;
	}

	function allTeam(){
		$this->db
		->from('team')
		->join('team_type','team_type.id_tt=team.id_tt');
        $query = $this->db->get();
		return $query;
	}

	function getTeam($id_team){
		$this->db
		->from('team')
		->where('id_team',$id_team);
        $query = $this->db->get();
		return $query;
	}

	function teamManuf(){
		$this->db
		->from('team')
		->where('team.id_tt','1')
		->join('team_type','team_type.id_tt=team.id_tt');
        $query = $this->db->get();
		return $query;
	}

	function rankManuf(){
		$this->db
		->select('saldo_akhir.*, team.*, (saldo_akhir.hold+saldo_akhir.simpanan-(saldo_akhir.pinjaman)) as total')
		->from('saldo_akhir')
		->join('team','team.id_team=saldo_akhir.id_team')
		->where('team.id_tt','1')
		->order_by('total','desc');
        $query = $this->db->get();
		return $query;
	}

	function teamOutsrc(){
		$this->db
		->from('team')
		->where('team.id_tt','2')
		->join('team_type','team_type.id_tt=team.id_tt');
        $query = $this->db->get();
		return $query;
	}

	function rankOutsrc(){
		$this->db
		->select('saldo_akhir.*, team.*, (saldo_akhir.hold+saldo_akhir.simpanan-(saldo_akhir.pinjaman)) as total')
		->from('saldo_akhir')
		->join('team','team.id_team=saldo_akhir.id_team')
		->where('team.id_tt','2')
		->order_by('total','desc');
        $query = $this->db->get();
		return $query;
	}


	function teamLog(){
		$this->db
		->from('team')
		->where('team.id_tt','3')
		->join('team_type','team_type.id_tt=team.id_tt');
        $query = $this->db->get();
		return $query;
	}

	function rankLog(){
		$this->db
		->select('saldo_akhir.*, team.*, (saldo_akhir.hold+saldo_akhir.simpanan-(saldo_akhir.pinjaman)) as total')
		->from('saldo_akhir')
		->join('team','team.id_team=saldo_akhir.id_team')
		->where('team.id_tt','3')
		->order_by('total','desc');
        $query = $this->db->get();
		return $query;
	}


	function teamLogOutsrc(){
		$this->db
		->from('team')
		->where('team.id_tt !=','1')
		->join('team_type','team_type.id_tt=team.id_tt');
        $query = $this->db->get();
		return $query;
	}

	function allProduct(){
		$this->db
		->from('product');
        $query = $this->db->get();
		return $query;
	}

	function allRawmat(){
		$this->db
		->from('rawmaterial');
        $query = $this->db->get();
		return $query;
	}

	function allPaket(){
		$this->db
		->from('paket');
        $query = $this->db->get();
		return $query;
	}

	function allIklan(){
		$this->db
		->from('iklan_type');
        $query = $this->db->get();
		return $query;
	}

	function getIklanType($id){
		$this->db
		->from('iklan_type')
		->where('id_iklan', $id);
        $query = $this->db->get();
		return $query;
	}

	function getPIklan($id){
		$this->db
		->from('iklan')
		->where('id_pIklan', $id);
        $query = $this->db->get();
		return $query;
	}

	function getDenda($id){
		$this->db
		->from('denda')
		->where('id_denda', $id);
        $query = $this->db->get();
		return $query;
	}

	function getPaket($id){
		$this->db
		->from('paket')
		->where('id_pkt', $id);
        $query = $this->db->get();
		return $query;
	}

	function getBMtrl($id){
		$this->db
		->from('beli_material')
		->where('id_bMtrl', $id);
        $query = $this->db->get();
		return $query;
	}

	function getRawmat($id){
		$this->db
		->from('rawmaterial')
		->where('id_raw', $id);
        $query = $this->db->get();
		return $query;
	}

	function getJualMaterial($id){
		$this->db
		->from('jual_material')
		->where('id_jMtrl', $id);
        $query = $this->db->get();
		return $query;
	}

	function getDetailJMtrl($id){
		$this->db
		->from('detail_jmtrl')
		->where('id_jMtrl', $id);
        $query = $this->db->get();
		return $query;
	}

	function getTabungan($id){
		$this->db
		->from('tabungan')
		->where('id_tab', $id);
        $query = $this->db->get();
		return $query;
	}

	function getPinjaman($id){
		$this->db
		->from('pinjaman')
		->where('id_pjm', $id);
        $query = $this->db->get();
		return $query;
	}

	function getPjmTeam($id){
		$this->db
		->from('pinjaman')
		->where('pinjaman.id_team', $id)
		->join('team','team.id_team=pinjaman.id_team');
        $query = $this->db->get();
		return $query;
	}

	function getSewa($id){
		$this->db
		->from('sewa')
		->where('id_sewa', $id);
        $query = $this->db->get();
		return $query;
	}

	function getDemandPrm($id_cty,$id_trm){
		$this->db
		->from('demand')
		->where('id_cty', $id_cty)
		->where('id_trm', $id_trm)
		->where('id_prd', 2);
        $query = $this->db->get();
		return $query;
	}

	function getPricePrm($id_cty,$id_trm){
		$this->db
		->from('price')
		->where('id_cty', $id_cty)
		->where('id_trm', $id_trm)
		->where('id_prd', 2);
        $query = $this->db->get();
		return $query;
	}

	function getDemandReg($id_cty,$id_trm){
		$this->db
		->from('demand')
		->where('id_cty', $id_cty)
		->where('id_trm', $id_trm)
		->where('id_prd', 1);
        $query = $this->db->get();
		return $query;
	}

	function getPriceReg($id_cty,$id_trm){
		$this->db
		->from('price')
		->where('id_cty', $id_cty)
		->where('id_trm', $id_trm)
		->where('id_prd', 1);
        $query = $this->db->get();
		return $query;
	}

	function sumDetail($id){
        $query = $this->db->query('SELECT SUM(jPrice) AS cum FROM detail_jmtrl where id_jMtrl = '.$id);
		return $query;
	}


	function allDemand(){
		$this->db
		->from('demand')
		->join('cities','cities.id_cty=demand.id_cty')
		->join('product','product.id_prd=demand.id_prd')
		->join('termin','termin.id_trm=demand.id_trm');
        $query = $this->db->get();
		return $query;
	}

	function allDemandTrm($id_trm){
		$this->db
		->from('demand')
		->join('cities','cities.id_cty=demand.id_cty')
		->join('product','product.id_prd=demand.id_prd')
		->join('termin','termin.id_trm=demand.id_trm')
		->where('demand.id_trm', $id_trm);
        $query = $this->db->get();
		return $query;
	}

	function allSaldo(){
		$this->db
		->from('saldo_akhir')
		->join('team','team.id_team=saldo_akhir.id_team');
        $query = $this->db->get();
		return $query;
	}

	function allPrice(){
		$this->db
		->from('price')
		->join('cities','cities.id_cty=price.id_cty')
		->join('product','product.id_prd=price.id_prd')
		->join('termin','termin.id_trm=price.id_trm');
        $query = $this->db->get();
		return $query;
	}

	function tc_frto($fr, $to, $trm){
		$this->db
		->from('transportation_cost')
		->where('fromcty',$fr)
		->where('tocty',$to)
		->where('id_trm',$trm);
        $query = $this->db->get();
		return $query;
	}

	function regPrice($to, $trm){
		$this->db
		->from('price')
		->where('id_cty',$to)
		->where('id_trm',$trm)
		->where('id_prd','1');
        $query = $this->db->get();
		return $query;
	}

	function prmPrice($to, $trm){
		$this->db
		->from('price')
		->where('id_cty',$to)
		->where('id_trm',$trm)
		->where('id_prd','2');
        $query = $this->db->get();
		return $query;
	}


	function allTransCost(){
		$this->db
		->from('transportation_cost')
		->join('termin','termin.id_trm=transportation_cost.id_trm');
        $query = $this->db->get();
		return $query;
	}

	function getLabLevel($user){
		$this->db
		->from('system_user_lab')
		->where('id_user',$user);
        $query = $this->db->get();
		return $query;
	}
	function getLab($lab){
		$this->db
		->from('laboratory')
		->where('id_lab',$lab);
        $query = $this->db->get();
		return $query;
	}
	function requestOprec($data){
		$this->db->insert('oprec', $data);
	}
	function allOprec(){
		$this->db
		->from('oprec')
		->join('laboratory','laboratory.id_lab=oprec.id_lab');
        $query = $this->db->get();
		return $query;
	}
	function getOprec($id){
		$this->db
		->from('oprec')
		->where('id_oprec',$id);
		$query = $this->db->get();
		return $query;
	}
	function getCandidateByLab($idlab){
		$this->db
		->from('oprec_general')
		->join('mahasiswa','mahasiswa.nim=oprec_general.nim')
		->where('oprec_general.id_lab',$idlab);
		$query = $this->db->get();
		return $query;
	}
	function getCandidate($nim){
		$this->db
		->from('candidate')
		->join('mahasiswa','mahasiswa.nim=candidate.nim')
		->where('candidate.nim',$nim);
		$query = $this->db->get();
		return $query;
	}
	function checkOG($idoprec,$nim){
		$this->db
		->from('oprec_general')
		->where('id_oprec',$idoprec)
		->where('nim',$nim);
		$query = $this->db->get();
		return $query;
	}
	function getOG($idoprec){
		$this->db
		->from('oprec_general')
		->where('id_oprec',$idoprec);
		$query = $this->db->get();
		return $query;
	}
	function getOGByOG($idog){
		$this->db
		->from('oprec_general')
		->join('oprec_status','oprec_general.oprec_status=oprec_status.id_os')
		->where('oprec_general.id_oprec_gen',$idog);
		$query = $this->db->get();
		return $query;
	}
	function getOGByNIM($idoprec){
		$this->db
		->from('oprec_general')
		->join('laboratory','laboratory.id_lab=oprec_general.id_lab')
		->where('oprec_general.nim',$idoprec);
		$query = $this->db->get();
		return $query;
	}
	function requestStep1($data){
		$this->db->insert('oprec_general', $data);
	}
	function requestUpdateStep1($data,$id) {
		$this->db->update('oprec_general',$data,array('id_oprec_gen'=>$id));
	}
	function requestStep2($data){
		$this->db->insert('oprec_education', $data);
	}
	function getOprecEdu($idog){
		$this->db
		->from('oprec_education')
		->where('id_oprec_gen',$idog)
		->order_by('year','asc');
		$query = $this->db->get();
		return $query;
	}
	function getOprecOrg($idog){
		$this->db
		->from('oprec_organization')
		->where('id_oprec_gen',$idog)
		->order_by('year','asc');
		$query = $this->db->get();
		return $query;
	}
	function requestStep3($data){
		$this->db->insert('oprec_organization', $data);
	}
	function allSoftLvl(){
		$this->db
		->from('software_lvl');
        $query = $this->db->get();
		return $query;
	}
	function requestStep4($data){
		$this->db->insert('oprec_skills', $data);
	}
	function getOprecSkills($idog){
		$this->db
		->from('oprec_skills')
		->join('software_lvl','oprec_skills.id_oprec_skills=software_lvl.id_sl')
		->where('id_oprec_gen',$idog);
		$query = $this->db->get();
		return $query;
	}
	function requestStep5($data){
		$this->db->insert('oprec_achievement', $data);
	}
	function getOprecAch($idog){
		$this->db
		->from('oprec_achievement')
		->join('achievement_type','achievement_type.id_at=oprec_achievement.achievement_type')
		->where('id_oprec_gen',$idog);
		$query = $this->db->get();
		return $query;
	}
	function allAchievementType(){
		$this->db
		->from('achievement_type');
        $query = $this->db->get();
		return $query;
	}
	function requestStep6($data){
		$this->db->insert('oprec_file', $data);
	}
	function requestUpdateStep6($data,$id) {
		$this->db->update('oprec_file',$data,array('id_oprec_gen'=>$id));
	}
	function getOprecFile($idog){
		$this->db
		->from('oprec_file')
		->where('id_oprec_gen',$idog);
		$query = $this->db->get();
		return $query;
	}
	function deleteEnroll($id) {
		$this->db->delete('oprec', array('id_oprec'=>$id));
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function getPenjualan($id){
		$this->db
		->from('penjualan')
		->where('id_pjl',$id);
        $query = $this->db->get();
		return $query;
	}

	function getKoran($id){
		$this->db
		->from('koran')
		->where('id_koran',$id);
        $query = $this->db->get();
		return $query;
	}

	function getSaldoTeam($id){
		$this->db
		->from('saldo_akhir')
		->where('id_team',$id);
        $query = $this->db->get();
		return $query;
	}




	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function allGoods(){
		$this->db
		->from('goods');
        $query = $this->db->get();
		return $query;
	}
	function requestEntryGoods($data){
		$this->db->insert('goods', $data);
	}
	function getGoods2($cd){
		$this->db
		->from('goods')
		->like('goods_code',$cd);
		$query = $this->db->get();
		return $query;
	}
	function getGoods($cd){
		$this->db
		->from('goods')
		->where('goods_code',$cd);
		$query = $this->db->get();
		return $query;
	}
	function requestEditEntryGoods($data,$cd) {
		$this->db->update('goods',$data,array('goods_code'=>$cd));
	}
	function deleteGoods($cd) {
		$this->db->delete('goods', array('goods_code'=>$cd));
	}
	function allUnit(){
		$this->db
		->from('unit')
		->join('unit_type','unit_type.id_ut=unit.unit_type');
        $query = $this->db->get();
		return $query;
	}
	function allUnitType(){
		$this->db
		->from('unit_type');
        $query = $this->db->get();
		return $query;
	}
	function getUnit($type){
		$this->db
		->from('unit')
		->where('unit_type',$type);
        $query = $this->db->get();
		return $query->result();
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function allTMaster(){
		$this->db
		->from('transaction_master')
		->join('unit','unit.id_unit=transaction_master.id_unit')
		->join('transaction_type','transaction_type.id_tt=transaction_master.transaction_type');
        $query = $this->db->get();
		return $query;
	}
	function addTMaster($data){
		$this->db->insert('transaction_master', $data);
	}
	function getIncrement($dt,$idunit){
		$this->db
		->from('transaction_master')
		->where('transaction_date',$dt)
		->where('id_unit',$idunit);
        $query = $this->db->get();
		return $query;
	}
	function getTCode($kod){
		$this->db
		->from('transaction_master')
		->join('unit','unit.id_unit=transaction_master.id_unit')
		->join('transaction_type','transaction_type.id_tt=transaction_master.transaction_type')
		->where('transaction_master.transaction_code',$kod);
        $query = $this->db->get();
		return $query;
	}
	function getTDetail($kod){
		$this->db
		->from('transaction_detail')
		->join('goods','goods.goods_code=transaction_detail.goods_code')
		->where('transaction_detail.transaction_code',$kod);
        $query = $this->db->get();
		return $query;
	}
	function addTDetail($data){
		$this->db->insert('transaction_detail', $data);
	}
	function deleteWithdrawal($id){
		$this->db->delete('transaction_detail',array('id_td'=>$id));
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function getMenu($level){
		$this->db
		->from('system_user_privileges')
		->join('system_menu','system_user_privileges.id_pm=system_menu.id_pm')
		->join('system_module','system_user_privileges.id_pm=system_module.id_pm')
		->where('system_menu.parent_id',0)
		//->where('system_module.status',1)
		->where('system_user_privileges.id_level',$level)
		->order_by('menu_order');
        $query = $this->db->get();
		return $query;
	}
	function getChildMenu($id){
		$this->db
		->from('system_menu')
		->where('parent_id',$id)
		->order_by('menu_order');
		$query = $this->db->get();
		return $query;
	}
}