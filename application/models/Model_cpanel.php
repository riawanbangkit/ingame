<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_cpanel extends CI_Model {
	function checkUserLogin($user,$pass){
		$this->db
		->from('view_system_user')
		->where('username',$user)
		->where('password',$pass);
        $query = $this->db->get();
		return $query;
	}
	function getUser($iduser){
		$this->db
		->from('view_system_user')
		->where('id_user',$iduser);
        $query = $this->db->get();
		return $query;
	}
	function requestAccount($data){
		$this->db->insert('system_user', $data);
	}
	
	function resetUsername() {
		$this->db->delete('system_user', array('level'=>'9'));
	}
	function allLevel(){
		$this->db
		->from('system_user_level');
        $query = $this->db->get();
		return $query;
	}
	function getLevel($id){
		$this->db
		->from('system_user_level')
		->where('id_level',$id);
        $query = $this->db->get();
		return $query;
	}
	function allModule(){
		$this->db
		->from('system_module');
        $query = $this->db->get();
		return $query;
	}
	function allAccount(){
		$this->db
		->from('view_system_user');
        $query = $this->db->get();
		return $query;
	}
	function getAccount($id){
		$this->db
		->from('system_user')
		->join('system_user_level','system_user_level.id_level=system_user.level')
		->where('system_user.id_user',$id);
        $query = $this->db->get();
		return $query;
	}
	function requestUpdateAccount($data,$id) {
		$this->db->update('system_user',$data,array('id_user'=>$id));
	}
	function deleteAccount($id) {
		$this->db->delete('system_user', array('id_user'=>$id));
	}
	function parentMenu(){
		$this->db
		->from('system_menu')
		//->join('system_module','system_module.id_pm=system_menu.id_pm')
		->where('system_menu.parent_id','0');
        $query = $this->db->get();
		return $query;
	}
	function childMenu($id){
		$this->db
		->from('system_menu')
		->join('system_module','system_module.id_pm=system_menu.id_pm')
		->where('system_menu.parent_id',$id);
        $query = $this->db->get();
		return $query;
	}
	function getMenu($id){
		$this->db
		->from('view_system_menu')
		->where('id',$id);
        $query = $this->db->get();
		return $query;
	}
	function requestMenu($data){
		$this->db->insert('system_menu', $data);
	}
	function updateMenu($data,$id) {
		$this->db->update('system_menu',$data,array('id'=>$id));
	}
	function deleteMenu($id) {
		$this->db->delete('system_menu', array('id'=>$id));
	}
	function getRights($id){
		$this->db
		->from('view_system_user_right_module')
		->where('id_level',$id);
        $query = $this->db->get();
		return $query;
	}
	function deleteRights($id) {
		$this->db->delete('system_user_rights', array('id_up'=>$id));
	}
	function requestAccessRights($data){
		$this->db->insert('system_user_rights', $data);
	}
}