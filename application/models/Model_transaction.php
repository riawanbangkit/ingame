<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_transaction extends CI_Model {

	function inputPenjualan($data){
		$this->db->insert('penjualan', $data);
	}

	function inputKoran($data){
		$this->db->insert('koran', $data);
	}

	function inputIklan($data){
		$this->db->insert('iklan', $data);
	}

	function inputDenda($data){
		$this->db->insert('denda', $data);
	}

	function updateSaldo($data,$team) {
		$this->db->update('saldo_akhir',$data,array('id_team'=>$team));
	}

	function inputPembelianMtrl($data){
		$this->db->insert('beli_material', $data);
	}

	function inputPenjualanMtrl($data){
		$this->db->insert('jual_material', $data);
	}

	function inputDetailPenjualanMtrl($data){
		$this->db->insert('detail_jMtrl', $data);
	}

	function updateJualMaterial($data,$id) {
		$this->db->update('jual_material',$data,array('id_jMtrl'=>$id));
	}

	function menabung($data){
		$this->db->insert('tabungan', $data);
	}

	function pinjam($data){
		$this->db->insert('pinjaman', $data);
	}

	function pelunasan($data,$id) {
		$this->db->update('pinjaman',$data,array('id_pjm'=>$id));
	}

	function inputSewa($data){
		$this->db->insert('sewa', $data);
	}

	function updateDemandPrm($data,$id_cty,$id_trm) {
		$this->db->update('demand',$data,array('id_cty'=>$id_cty, 'id_trm'=>$id_trm, 'id_prd'=>'2'));
	}

	function updateDemandReg($data,$id_cty,$id_trm) {
		$this->db->update('demand',$data,array('id_cty'=>$id_cty, 'id_trm'=>$id_trm, 'id_prd'=>'1'));
	}

}