<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_general extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	function getMenu($level){
		$this->db
		->from('view_system_user_rights')
		->where('parent_id',0)
		->where('id_level',$level)
		->order_by('menu_order');
        $query = $this->db->get();
		return $query;
	}
	function getChildMenu($id){
		$this->db
		->from('system_menu')
		->where('parent_id',$id)
		->order_by('menu_order');
		$query = $this->db->get();
		return $query;
	}
	function getUserRole($iduser){
		$this->db
		->from('view_system_user_role')
		->where('id_user',$iduser);
		$query = $this->db->get();
		return $query;
	}
	
	function requestChangeRole($iduser,$data){
		$this->db->update('system_user',$data,array('id_user'=>$iduser));
	}
}